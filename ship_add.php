<?php 
  require 'header.php';
?>   
<style> 
  #user_data1_info,#user_data1_length{float:left}#user_data1_filter,#user_data1_paginate{float:right}
  #user_data2_info,#user_data2_length{float:left}#user_data2_filter,#user_data2_paginate{float:right}
.table-hover tbody tr:hover td,.table-hover tbody tr:hover th{background-color:#ffedda}.table td{vertical-align:middle!important;font-size:11px!important;color:#000;font-family:Verdana,Geneva,sans-serif;padding-top:4px;padding-right:4px;padding-bottom:4px;padding-left:10px}.table-bordered td{border:3px solid #e3e6f0}#user_data_info,#user_data_length{float:left}#user_data_filter,#user_data_paginate{float:right}.paginate_button{color:#000;float:left;padding:6px 12px;text-decoration:none;border:1px solid #ccc;cursor:pointer}.ellipsis{display:none}[type=search]{margin-right:10px; width: 250px; }.ui-autocomplete{z-index:2150000000!important}.container input{position:absolute;opacity:0;cursor:pointer;height:0;width:0}.checkmark{border-radius:2px;position:absolute;top:0;height:20px;width:20px;background-color:#fff;border:1px solid #000}.container:hover input~.checkmark{background-color:#fff}.container input:checked~.checkmark{background-color:#fff}.container input:disabled~.checkmark{background-color:#eaecf4}.checkmark:after{content:"";position:absolute;display:none}.container input:checked~.checkmark:after{display:block}.container .checkmark:after{left:6px;top:-1px;width:8px;height:16px;border:solid #000;border-width:0 3px 3px 0;-webkit-transform:rotate(45deg);-ms-transform:rotate(45deg);transform:rotate(45deg)}button:disabled,button[disabled]{border:1px solid #333!important;color:#333!important;cursor:no-drop} .table .thead-light th{text-align: center; font-size: 11px; color:#444; text-transform: uppercase; } .component{display: none;} 
  table {width: 100% !important;} table.table-bordered.dataTable td { white-space: nowrap; overflow: hidden; text-overflow:ellipsis;  }

  /* This css is for normalizing styles. You can skip this. */
*, *:before, *:after {
  -webkit-box-sizing: border-box;
  -moz-box-sizing: border-box;
  box-sizing: border-box;
  margin: 0;
  padding: 0;
}


td:nth-child(1){
  padding: 0px !important;
  width: 120px;
}

.new {
/*  padding: 50px;
*/}

.form-group {
  display: block;
  margin-bottom: 15px;
}

.form-group input {
  padding: 0;
  height: initial;
  width: initial;
  margin-bottom: 0;
  display: none;
  cursor: pointer;
}

.form-group label {
  position: relative;
  cursor: pointer;
}

.form-group label:before {
  content:'';
  -webkit-appearance: none;
  background-color: transparent;
  border: 2px solid #004973;
  background-color: #FBFBFB;
  box-shadow: 0 1px 2px rgba(0, 0, 0, 0.05), inset 0px -15px 10px -12px rgba(0, 0, 0, 0.05);
  padding: 8px;
  display: inline-block;
  position: relative;
  vertical-align: middle;
  cursor: pointer;
  margin-right: 5px;
}

.form-group input:checked + label:after {
  content: '';
  display: block;
  position: absolute;
  top: 2px;
  left: 7px;
  width: 6px;
  height: 13px;
  border: solid #004973;
  border-width: 0 2px 2px 0;
  transform: rotate(45deg);
}

input{
  text-transform: uppercase;
}
 
#appenddiv, #appenddiv2, #appenddiv3 {
    display: block; 
    position:relative
} 
.ui-autocomplete {
    position: absolute;
}

.card{
  border-radius: 0px;
}

table{
  /*margin: 0px !important;*/
}
</style>
  

<div id="response"></div> 


 <div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="memberModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-md">
        <div class="modal-content"> 
            <div class="dash" id="modaldetail"> 
            </div> 
        </div>
    </div>
 </div>


<div class="row"> 
<div class="col-md-12"> <h3 style="float: left; margin-top: 10px;"> Add New Shipment (पोहोच, बिल्टी, ट्रिप के अलावा किसी और वास्तु को भेजने के लिए शिपमेंट बनाये) </h3> 

</div>
 
<div class="col-md-12" >  
<div class="card shadow mb-4"> 
<div class="card-body">
<div class="table-responsive">
  
<form id="save" action="" method="post" autocomplete="off">


<div class="col-md-12" style="padding-bottom: 12px;">
<div class="row">

  <div class="col-md-2">
          <label style="text-transform: uppercase; color: #444;">Shipment Date </label>
          <input oninput="this.value=this.value.replace(/[^a-z 0-9 A-Z.&-]/,'')" type="date" class="form-control" id="" name="date" value="" required="" min="<?php echo date('Y-m-d',strtotime("-2 days")); ?>" max="<?php echo date('Y-m-d'); ?>" />
    </div>

<div class="col-md-2">
          <label style="text-transform: uppercase; color: #444;"> Direct Dispatch </label>
          <select class="form-control" name="dd" id="dd" required="" onChange="direct(this);">
            <option value=""> --  select -- </option>
            <option value="1">Yes</option>
            <option value="0">No</option>
          </select>

</div>


<div class="col-md-2">
          <label style="text-transform: uppercase; color: #444;"> Outstanding Intermemo </label>
          <select class="form-control" name="memo" id="memo" disabled="">
            <option value=""> --  select -- </option>
             <?php
             $sql = "select * from podmemo where branch='$branchuser' and status='0' order by id desc";
             $res = $conn->query($sql);
             while($row = $res->fetch_assoc()){

                $narration = "NA";
              if($row["sentby"]=="COURIER"){
                $narration = "Courier Name: ".$row['couriername']." / Docket no: ".$row['docketno'];
              } else if($row["sentby"]=="TRUCK"){
                $narration = "Truck No: ".$row['truckno']." / Driver Name: ".$row['drivername']." / Driver Mobile: ".$row['drivermobile'];
              } else if($row["sentby"]=="PERSON"){
                $narration = "Person Name: ".$row['contactname']." / Person Mobile: ".$row['contactmobile'];
              } else if($row["sentby"]=="OTHERS"){
                $narration = "Others: ".$row['narration'];
              }

              echo "<option value='$row[memono]'> $row[memono] (".ucfirst(strtolower($row['branch']))." to ".ucfirst(strtolower($row['bill_branch'])).") Via $narration </option>";
             }
             ?>
          </select>

</div>


<div class="col-md-2">
            <label style="text-transform: uppercase; color: #444;"> From Station </label>
            <input oninput="this.value=this.value.replace(/[^a-z 0-9 A-Z.&-]/,'')" type="text" class="form-control" id="fstat" name="fstat" value="" disabled=""/>
            <div id="appenddiv2"></div>

</div>

<div class="col-md-2">
            <label style="text-transform: uppercase; color: #444;"> To Station </label>
            <input oninput="this.value=this.value.replace(/[^a-z 0-9 A-Z.&-]/,'')" type="text" class="form-control" id="tstat" name="tstat" value="" disabled=""/>
            <div id="appenddiv3"></div>
</div>


    <div class="col-md-2 ">
    <label style="text-transform: uppercase; color: #444;"> Dispatch Via </label>
    <select id="sendby" class="form-control" name="sendby"  onChange="updt(this);" disabled="">
      <option value=""> -- select -- </option>
      <option value="COURIER"> COURIER </option>
      <option value="TRUCK"> TRUCK  </option> 
      <option value="PERSON"> PERSON </option> 
      <option value="OTHERS"> OTHERS </option> 
    </select>
    </div>
</div>
</div>


    <div class="col-md-12" style="padding-bottom: 12px;">
    <div class="row">



  
 
    <div class="col-md-2" class="display1">
    <label style="text-transform: uppercase; color: #444;"> Courier  </label>
    <select id="val10" name="val10" class="form-control" disabled="" onChange="maruti(this);">
        <option value=""> -- select -- </option>
        <option value="Shree Maruti Courier">Shree Maruti Courier</option>
        <option value="others"> Others </option>        
    </select>
    </div>

    <div class="col-md-2" class="display1">
          <label style="text-transform: uppercase; color: #444;"> Courier Name </label>
          <input oninput="this.value=this.value.replace(/[^a-z 0-9 A-Z.&-]/,'')" type="text" class="form-control" id="val1" name="val1" value="" readonly="" />
    </div>
 
    <div class="col-md-2" class="display2" >
          <label style="text-transform: uppercase; color: #444;"> Have Docket No ? </label>
          <select id="doc" name="doc" class="form-control" disabled="" onChange="updtdoc(this);"> 
            <option value=""> -- select -- </option>
            <option value="YES"> Yes </option>
            <option value="NO"> No </option>
          </select>
    </div> 

    <div class="col-md-2" class="display1">
          <label style="text-transform: uppercase; color: #444;"> Docket No </label>
          <input oninput="this.value=this.value.replace(/[^a-z 0-9 A-Z.&-]/,'')" type="text" class="form-control" id="val2" name="val2" value="" disabled=""/>
    </div>

  <div class="col-md-2" class="display2" >
          <label style="text-transform: uppercase; color: #444;"> Contact Name </label>
          <input oninput="this.value=this.value.replace(/[^a-z 0-9 A-Z.&-]/,'')" type="text" class="form-control" id="val3" name="val3" value="" disabled="" />
    </div> 

    <div class="col-md-2" class="display2">
          <label style="text-transform: uppercase; color: #444;"> Contact Mobile No </label>
          <input oninput="this.value=this.value.replace(/[^a-z 0-9 A-Z.&-]/,'')" type="text" class="form-control" id="val4" name="val4" value="" disabled="" />
    </div> 
 
    </div>
    </div>
    <div class="col-md-12" style="padding-bottom: 20px;">
    <div class="row">

    <div class="col-md-2" class="display3" >
          <label style="text-transform: uppercase; color: #444;"> Truck No </label>
          <input oninput="this.value=this.value.replace(/[^a-z 0-9 A-Z.&-]/,'')" type="text" class="form-control" id="val5" name="val5" value="" disabled="" />
          <div id="appenddiv"></div>
    </div> 

    <div class="col-md-2" class="display3" >
          <label style="text-transform: uppercase; color: #444;"> Driver Name </label>
          <input oninput="this.value=this.value.replace(/[^a-z 0-9 A-Z.&-]/,'')" type="text" class="form-control" id="val6" name="val6" value="" disabled="" />
    </div>

    <div class="col-md-2" class="display3" >
          <label style="text-transform: uppercase; color: #444;"> Driver Mobile </label>
          <input oninput="this.value=this.value.replace(/[^a-z 0-9 A-Z.&-]/,'')" type="text" class="form-control" id="val7" name="val7" value="" disabled="" />
    </div>

    <input type="hidden" name="billbranch" oninput="this.value=this.value.replace(/[^a-z 0-9 A-Z.&-]/,'')" value="<?php echo $id; ?>">
    <input type="hidden" name="billparty" oninput="this.value=this.value.replace(/[^a-z 0-9 A-Z.&-]/,'')" value="0">
    <input type="hidden" name="billpartyid" oninput="this.value=this.value.replace(/[^a-z 0-9 A-Z.&-]/,'')" value="0">


    <div class="col-md-2" class="display4" >
          <label style="text-transform: uppercase; color: #444;"> NARRATION </label>
          <input oninput="this.value=this.value.replace(/[^a-z 0-9 A-Z.&-]/,'')" type="text" class="form-control" id="val8" name="val8" value="" disabled="" />

    </div>


 
      <div class="col-md-4" >
      <table class="table table-hover order-list">
          
          <tr>
            <td  style="width: 100%;"></td>
            <td><button type="button" style="" class="addbtn btn btn-success btn-xs" id="addrow"> <i style="" class="fa fa-plus" aria-hidden="true"></i> Add Item </button></td>
          </tr>
          <tr class="isi">
          </tr>
          
        </table>
      </div> 

          <div class="col-md-2" class="display3" >
    <button style="margin-top: 20px; letter-spacing: 1px; width: 100%;" type="submit" class=' btn btn-success'> <i class='fa fa-truck '></i> <b> DISPATCH </b> </button>    
    </div>
</div>
    </div>

<script type="text/javascript"> 
$(document).ready(function () {
    var counter = 1;
    var nos = 1;
    $("#addrow").on("click", function () {
        nos = nos + 1;
        var newRow = $("<tr>");
        var cols = "";  
        cols += '<td><input class="form-control" type="text" style="" name="item[]" autocomplete="off" placeholder="Item description" required></td> ';
        
        cols += '<td> <button type="button"  class="btn-xs ibtnDel btn btn-md btn-danger" value="-"> <i style="" class="fa fa-times" aria-hidden="true"></i> </button></td>';
        newRow.append(cols);
        newRow.insertBefore("tr.isi");
 
        counter++;
    });
 
    $("table.order-list").on("click", ".ibtnDel", function (event) {
        $(this).closest("tr").remove();      
        counter -= 1;
    });
}); 
</script>


  </form>

</div>

  
</div>  
</div>
</div> 
</div>  
 
 <script type="text/javascript">
   
  function maruti(sel){

    if(sel.value == "others"){
      $("#val1").prop('required', true);
      $("#val1").prop('readonly', false);
      $("#val1").val('');
    } else {
      $("#val1").prop('required', false);
      $("#val1").prop('readonly', true);
      $("#val1").val('SHREE MARUTI COURIER');
    }

  }

  function updtdoc(sel){

    if(sel.value == "YES"){
      $("#val2").prop('required', true);
      $("#val2").prop('disabled', false);
    } else {
      $("#val2").prop('required', false);
      $("#val2").prop('disabled', true);
    }

  }

  function updt(sel) {
  
    if(sel.value == "COURIER")
    {
      $("#val10").prop('required',true);
      $("#val10").prop('disabled',false);
      $("#doc").prop('required',true);
      $("#doc").prop('disabled',false);      
    } else {
      $("#val10").prop('required',false);
      $("#val10").prop('disabled',true);
      $("#doc").prop('required',false);
      $("#doc").prop('disabled',true);
  }
    if(sel.value == "TRUCK")
    {
      $("#val5").prop('required',true);
      $("#val5").prop('disabled',false);
      $("#val6").prop('required',true);
      $("#val6").prop('disabled',false);   
      $("#val7").prop('required',true);
      $("#val7").prop('disabled',false);  
    } else {
      $("#val5").prop('required',false);
      $("#val5").prop('disabled',true);
      $("#val6").prop('required',false);
      $("#val6").prop('disabled',true);
      $("#val7").prop('required',false);
      $("#val7").prop('disabled',true);
    }
    if(sel.value == "PERSON")
    {
      $("#val3").prop('required',true);
      $("#val3").prop('disabled',false);
      $("#val4").prop('required',true);
      $("#val4").prop('disabled',false);    
    } else { 
      $("#val3").prop('required',false);
      $("#val3").prop('disabled',true);
      $("#val4").prop('required',false);
      $("#val4").prop('disabled',true);
    }
    if(sel.value == "OTHERS")
    {
      $("#val8").prop('required',true);
      $("#val8").prop('disabled',false);
    } else {
      $("#val8").prop('required',false);
      $("#val8").prop('disabled',true);
    }
  } 

  function direct(sel){
    if(sel.value == "0")
    {
      $("#memo").prop('required',true);
      $("#memo").prop('disabled',false);

      $("#fstat").prop('required',false);
      $("#fstat").prop('disabled',true);
      $("#tstat").prop('required',false);
      $("#tstat").prop('disabled',true);      
      $("#sendby").prop('required',false);
      $("#sendby").prop('disabled',true);

    } else {
      $("#memo").prop('required',false);
      $("#memo").prop('disabled',true);

      $("#fstat").prop('required',true);
      $("#fstat").prop('disabled',false);
      $("#tstat").prop('required',true);
      $("#tstat").prop('disabled',false);
      $("#sendby").prop('required',true);
      $("#sendby").prop('disabled',false);
    }

  }

 </script>

  <script type="text/javascript">
    $(function() {
    $("#val5").autocomplete({
    source: 'pod_view_truck.php',
    appendTo: '#appenddiv',
    select: function (event, ui) { 
       $('#val5').val(ui.item.value);   
       $('#val6').val(ui.item.dname);
       $('#val7').val(ui.item.dcode);      
       return false;
    },
    change: function (event, ui) {
    if(!ui.item){
    $(event.target).val("");
    Swal.fire({
    icon: 'error',
    title: 'Error !!!',
    text: 'Truck does not exists !'
    })
    $("#val6").val("");
    $("#val7").val("");
    $("#val5").val("");
    $("#val5").focus();
    }
    }, 
    focus: function (event, ui){
    return false;
    }
    });
    });


    $(function() {
    $("#fstat").autocomplete({
    source: 'station.php',
    appendTo: '#appenddiv2',
    select: function (event, ui) { 
       $('#fstat').val(ui.item.value);   
        return false;
    },
    change: function (event, ui) {
    // if(!ui.item){
    // $(event.target).val("");
    // Swal.fire({
    // icon: 'error',
    // title: 'Error !!!',
    // text: 'Truck does not exists !'
    // })
    //  $("#fstat").val("");
    // $("#fstat").focus();
    // }
    }, 
    focus: function (event, ui){
    return false;
    }
    });
    });

    $(function() {
    $("#tstat").autocomplete({
    source: 'station.php',
    appendTo: '#appenddiv3',
    select: function (event, ui) { 
       $('#tstat').val(ui.item.value);   
        return false;
    },
    change: function (event, ui) {
    // if(!ui.item){
    // $(event.target).val("");
    // Swal.fire({
    // icon: 'error',
    // title: 'Error !!!',
    // text: 'Truck does not exists !'
    // })
    //  $("#fstat").val("");
    // $("#fstat").focus();
    // }
    }, 
    focus: function (event, ui){
    return false;
    }
    });
    });
  </script>

<script type="text/javascript"> 

// jQuery( document ).ready(function() {

// $('#loadicon').show(); 
// var table = jQuery('#user_data').dataTable({
//      "lengthMenu": [ [7, 500, 1000, -1], [7, 500, 1000, "All"] ], 
//      "bProcessing": true,
//      "sAjaxSource": "ship_fetch.php",
//       "bPaginate": true,
//       "sPaginationType":"full_numbers",
//       "iDisplayLength": 7,
//       //"order": [[ 8, "desc" ]],
//       "columnDefs":[
//       {
//         "targets":[0, 1, 2, 3, 4, 5, 6,7],
//         "orderable":false,
//       },
//       ],
//       "aoColumns": [
//     ],
//     "initComplete": function( settings, json ) {
//     $('#loadicon').hide();
//     }
// });  
 
// });  

    // function update(val){
    // $('#loadicon').show(); 
    //        var id = val;
    //        $.ajax({
    //             url:"doc_update.php",  
    //             method:"post",  
    //             data:{id:id},  
    //             success:function(data){  
    //                  $('#modaldetail').html(data);  
    //                  $('#exampleModal').modal("show");  
    //                  $('#loadicon').hide(); 
    //             }
    //        });
    // }


    // function view(val){
    // $('#loadicon').show(); 
    //        var id = val;
    //        $.ajax({
    //             url:"doc_view.php",  
    //             method:"post",  
    //             data:{id:id},  
    //             success:function(data){  
    //                  $('#modaldetail').html(data);  
    //                  $('#exampleModal').modal("show");  
    //                  $('#loadicon').hide(); 
    //             }
    //        });
    // }

    // $(document).on('submit', '#updatereq', function()
    // {
    // $('#loadicon').show(); 
    //   var data = $(this).serialize(); 
    //   $.ajax({
    //     type : 'POST',
    //     url  : 'doc_insert.php',
    //     data : data,
    //     success: function(data) {  
    //     document.getElementById("hidemodal").click();  
    //     $('#response').html(data);  
    //     $('#user_data').DataTable().ajax.reload();  
    //     $('#loadicon').hide(); 
    //     }
    //   });
    //   return false;
    // });


     $(document).on('submit', '#save', function()
    {  
    $('#loadicon').show(); 
      var data = $(this).serialize(); 
      $.ajax({  
        type : 'POST',
        url  : 'ship_save.php',
        data : data,
        success: function(data) {  
        // document.getElementById("hidemodal").click();  
        $('#response').html(data);  
        $('#loadicon').hide(); 
        $("#save")[0].reset();
        }
      });
      return false;
    });


</script>
<script type="text/javascript">
// $(document).ready(function() { 
//     var table = $('#user_data').DataTable(); 
// } );
</script>   
<?php 
  include 'footer.php';
?>  
 