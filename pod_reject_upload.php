<?php 
  
	require('connect.php');

	function sanitize($data) {
		$data = trim($data);
		$data = stripslashes($data);
		$data = htmlspecialchars($data);
		return $data;
	}  

 	$id =  $conn -> real_escape_string($_POST['post_id']); 

	$file_name = $_FILES['file']['name'];
	$file_size =$_FILES['file']['size'];
	$file_tmp =$_FILES['file']['tmp_name'];
	$file_type=$_FILES['file']['type'];
	@$file_ext=strtolower(end(explode('.',$_FILES['file']['name'])));

	$uploadid = strtoupper(chr(rand(65, 90)) . strtotime('now') . chr(rand(97,122)));
 
	$getname = substr(preg_replace("/[^0-9a-zA-Z]/", "", $_FILES['file']['name']), 0, -4);	
	$allowedExts = array("gif", "jpeg", "jpg", "png", "pdf"); 
	$temp = explode(".", $_FILES["file"]["name"]); 
	$extension = end($temp);
 
 	try {
	$conn->query("START TRANSACTION"); 

	$sql = "select * from rrpl_database.rcv_pod where id='$id'";
	if ($conn->query($sql) === FALSE) {
		$errorno = mysqli_error($conn);
		throw new Exception($errorno); 
	} 
	$resg = $conn->query($sql);
	$rowg = $resg->fetch_assoc();

	if ((($_FILES["file"]["type"] == "image/gif")
	|| ($_FILES["file"]["type"] == "image/jpeg")
	|| ($_FILES["file"]["type"] == "image/jpg")
	|| ($_FILES["file"]["type"] == "image/pjpeg")
	|| ($_FILES["file"]["type"] == "image/x-png")
	|| ($_FILES["file"]["type"] == "image/png")
	|| ($_FILES["file"]["type"] == "application/pdf") 
	&& in_array($extension, $allowedExts))){

	$newfile = $getname.$uploadid.".".$file_ext;	

	if($rowg['veh_type']=='MARKET'){
	$targetPath =  '../b5aY6EZzK52NA8F/pod_copy/'.$newfile; // path for market pod file
	$database_file_name = "pod_copy/".$newfile;

	} else if($rowg['veh_type']=='OWN'){
	$targetPath =  '../diary/close_trip/pod/'.$newfile; // path for own pod file
	$database_file_name = "pod/".$newfile;

	} else {
		throw new Exception("Invalid vehicle type !"); 
	}

	if(move_uploaded_file($file_tmp, $targetPath)){
  
			$sql = "insert into rrpl_database.exp_upload_log (vid,voucher,oldupload,newupload,type,fno) values ('$rowg[id]','$rowg[lrno]','$rowg[pod_copy]','$database_file_name','$rowg[veh_type]','$rowg[frno]')";
 			if ($conn->query($sql) === FALSE) {
				$errorno = mysqli_error($conn);
				throw new Exception($errorno); 
			}

			$sql = "update rrpl_database.rcv_pod set pod_copy='$database_file_name', ho_pod_check='0', reupload='1' where id='$id'";	 
			if ($conn->query($sql) === FALSE) {
				$errorno = mysqli_error($conn);
				throw new Exception($errorno); 
			} 

			if($rowg['veh_type']=='OWN'){
				$sql = "update dairy.rcv_pod set copy='$database_file_name' where vou_no='$rowg[frno]' and lrno='$rowg[lrno]'";	 
				if ($conn->query($sql) === FALSE) {
					$errorno = mysqli_error($conn);
					throw new Exception($errorno); 
				} 
			}

           
	} else {
			throw new Exception("Uploading Error - Something went wrong !");  
	}

	} else {
			throw new Exception("Please upload a valid image file !");  
	}
 
			$conn->query("COMMIT");
			echo "
			<script>
			Swal.fire({
			position: 'top-end',
			icon: 'success',
			title: 'Image Updated.',
			showConfirmButton: false,
			timer: 1000
			})
			</script>";

 	} catch(Exception $e) { 

			$conn->query("ROLLBACK"); 
			$content = $e->getMessage();
			$content = preg_replace("/[^0-9a-zA-Z ]/", "", $content);  
			echo "
			<script>
			Swal.fire({
			icon: 'error',
			title: 'Error !!!',
			text: '$content'
			})
			</script>";		
	} 
?>  