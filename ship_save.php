<?php
require('connect.php');
 
 
   $date = $conn->real_escape_string(strtoupper($_POST['date'])); // SHIPMENT DATE
   $dd = $conn->real_escape_string(strtoupper($_POST['dd'])); // DIRECT DISPATCH
  
if(isset($_POST['memo']))
{
   $memo = $conn->real_escape_string(strtoupper($_POST['memo'])); //
   $sqlm = "select * from podmemo where memono='$memo'";
   $resm = $conn->query($sqlm);
   $rowm = $resm->fetch_assoc();
} else {
   $memo = "NA";
}

if(isset($_POST['fstat']))
{
   $fstat = $conn->real_escape_string(strtoupper($_POST['fstat'])); //
} else {
   $fstat = $rowm['branch'];
}

if(isset($_POST['tstat']))
{
   $tstat = $conn->real_escape_string(strtoupper($_POST['tstat'])); //
} else {
   $tstat = $rowm['bill_branch'];
}

if(isset($_POST['sendby']))
{
   $sendby = $conn->real_escape_string(strtoupper($_POST['sendby'])); //
} else {
   $sendby = "";
}
 
if(isset($_POST['val1']))
{
  $val1 = $conn->real_escape_string(strtoupper($_POST['val1']));
} else {
  $val1 = "";
}

if(isset($_POST['val2']))
{
  $val2 = $conn->real_escape_string(strtoupper($_POST['val2']));
} else {
  $val2 = "";
}

if(isset($_POST['val3']))
{
  $val3 = $conn->real_escape_string(strtoupper($_POST['val3']));
} else {
  $val3 = "";
}

if(isset($_POST['val4']))
{
  $val4 = $conn->real_escape_string(strtoupper($_POST['val4']));
} else {
  $val4 = "";
}

if(isset($_POST['val5']))
{
  $val5 = $conn->real_escape_string(strtoupper($_POST['val5']));
} else {
  $val5 = "";
}

if(isset($_POST['val6']))
{
  $val6 = $conn->real_escape_string(strtoupper($_POST['val6']));
} else {
  $val6 = "";
}

if(isset($_POST['val7']))
{
  $val7 = $conn->real_escape_string(strtoupper($_POST['val7']));
} else {
  $val7 = "";
}

if(isset($_POST['val8']))
{
  $val8 = $conn->real_escape_string(strtoupper($_POST['val8']));
} else {
  $val8 = "";
}

	try {
	$conn->query("START TRANSACTION"); 
  
if(!empty($_POST['item']))
{ 

	if($sendby=="COURIER"){

		if(empty($val1)){
				throw new Exception(" Courier detail is Missing ! "); 
		}

	} else if($sendby=="TRUCK"){

		if(empty($val5) or empty($val6) or empty($val7)){
				throw new Exception(" Truck detail is Missing ! "); 
		}

	} else if($sendby=="PERSON"){

		if(empty($val3) or empty($val4)){
				throw new Exception(" Person detail is Missing ! "); 
		}

	} else if($sendby=="OTHERS"){

		if(empty($val8)){
				throw new Exception(" Others detail is Missing ! "); 
		}
	} 

		$todaydate = date('Y-m-d');
 		$sqli = $conn->query("select count(id) from shipment where date(dispatchdate)='$todaydate' ");
 		$resi = $sqli->fetch_assoc();
 		$getlast = $resi['count(id)']+1;
 
	  $narration = "NA";
	  if($sendby=="COURIER"){
	    $narration = "Courier Name: ".$val1." / Docket No: ".$val2;
	  } else if($sendby=="TRUCK"){
	    $narration = "Truck No: ".$val5." / Driver Name: ".$val6." / Driver Mobile: ".$val7;
	  } else if($sendby=="PERSON"){
	    $narration = "Person Name: ".$val3." / Person Mobile: ".$val4;
	  } else if($sendby=="OTHERS"){
	    $narration = "Others: ".$val8;
	  }

	  if($memo!='NA'){
	  $narration = "NA";
	  }
		
	  $shipmentno = "SHP".date('dmy').$getlast;

		  	$totalitem = "0";

		  	if(!empty($_POST['item'])){
				$totallr = count($_POST['item']); 
				$all_lrs = array(); 
				foreach ($_POST['item'] as $id) { 
					$all_lrs[] = strtoupper(htmlspecialchars($id)); 
				}
				$all_lrs = array_unique($all_lrs);
				$lrs = "('" . implode ( "',  '$shipmentno'), ('", $all_lrs ) . "',  '$shipmentno')";
				 $sql = "insert into shipment_item (item,shipno) values $lrs";
				if ($conn->query($sql) === FALSE) {
				throw new Exception("Error: Insert failed for Item !"); 
				}
			} else {
				$lrs = "";
			} 

	$sql = "update rrpl_database.podmemo set shipment=shipment+$totallr where memono='$memo'";
	if ($conn->query($sql) === FALSE) {
		throw new Exception("Error: update failed for Shipment count !"); 
	}
  
 	$sql = "INSERT INTO shipment (`dated`,`user`, `shipno`, `source`, `destination`, `dispatch`, `dispatchdate`, `dispatchvia`, `memono`, `dispatchuser`) VALUES ('$date','$branchuser', '$shipmentno', '$fstat', '$tstat', '1', '$sysdatetime', '$narration', '$memo' ,'$empid')";
	if ($conn->query($sql) === FALSE) {
		throw new Exception("Error: Insert failed in Shipment !"); 
	}
  	
  	if($memo!='NA'){

 	$sql = "insert into podtrack (`lrno`, `lrid`, `frno`, `lrtype`,  `dispatchbranch`, `collectbranch`, `dispatch`, `dispatchdate`, `dispatchusr`, `memono`) select '$shipmentno', id, '$shipmentno', 'SHIPMENT', source, destination, '1', '$sysdatetime', '$empid', memono from shipment where shipno='$shipmentno'";
	if ($conn->query($sql) === FALSE) {
		throw new Exception("Error: Insert failed in TRACK for Shipment !"); 
	}  		
  	} 

	} else {
		throw new Exception("Error: Please Add item !"); 
	}

		$conn->query("COMMIT");
		echo "
		<script>
		Swal.fire({
		position: 'top-end',
		icon: 'success',
		title: 'Shipment Updated.',
		showConfirmButton: false,
		timer: 1500
		})
		</script>"; 
		}
		catch(Exception $e) {
				$conn->query("ROLLBACK"); 
				$content = $e->getMessage();
				$content = preg_replace("/[^0-9a-zA-Z ]/", "", $content);  
				echo "
				<script>
				Swal.fire({
				icon: 'error',
				title: 'Error !!!',
				text: '".$content."'
				})
				</script>";
		}