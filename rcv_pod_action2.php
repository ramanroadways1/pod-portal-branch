<?php

require('connect.php'); 

  $id = $conn->real_escape_string($_REQUEST['id']); 
  $action = $conn->real_escape_string($_REQUEST['act']); 
  $memo = $conn->real_escape_string($_REQUEST['memo']); 

if($action=="select"){

	$qry = Qry($conn,"UPDATE rcv_pod SET collect='1', collect_time='$sysdatetime' WHERE id='$id' and dispatch='1' and (collect='0' or collect='-1' )");
	if(!$qry)
	{
		echo "
		<script>
		Swal.fire({
		icon: 'error',
		title: 'Error !!!',
		text: '".mysqli_error($conn)."'
		})
		</script>";  
		exit();
	} else {
		$qry = Qry($conn, "UPDATE podmemo set remainlr=remainlr-1, collectdate='$sysdatetime'  where memono='$memo'");
		if(!$qry)
		{
			echo "
			<script>
			Swal.fire({
			icon: 'error',
			title: 'Error !!!',
			text: '".mysqli_error($conn)."'
			})
			</script>";  
			exit();
		}else{ 
			echo "
			<script>
			Swal.fire({
			position: 'top-end',
			icon: 'success',
			title: 'LR Accepted.',
			showConfirmButton: false,
			timer: 1000
			})
			</script>"; 
		}
	}
	//$qry = Qry($conn,"insert into rcv_pod_log (rcv_pod_id, action, timestamp) values ('$id','ACCEPT','$sysdatetime')");  

} else if ($action=="reject"){

	$qry = Qry($conn,"UPDATE rcv_pod SET dispatch='0', collect='-1', collect_time=NULL WHERE id='$id' and dispatch='1' and (collect='0' or collect='-1' )");
	if(!$qry)
	{
		echo "
		<script>
		Swal.fire({
		icon: 'error',
		title: 'Error !!!',
		text: '".mysqli_error($conn)."'
		})
		</script>";  
		exit();
	}  else {
		$qry = Qry($conn, "UPDATE podmemo set remainlr=remainlr-1, collectdate='$sysdatetime' where memono='$memo'");
		if(!$qry)
		{
			echo "
			<script>
			Swal.fire({
			icon: 'error',
			title: 'Error !!!',
			text: '".mysqli_error($conn)."'
			})
			</script>";  
			exit();
		} else { 
			echo "
			<script>
			Swal.fire({
			position: 'top-end',
			icon: 'success',
			title: 'LR Cancelled.',
			showConfirmButton: false,
			timer: 1000
			})
			</script>"; 
		}
	}
	//$qry = Qry($conn,"insert into rcv_pod_log (rcv_pod_id, action, timestamp) values ('$id','CANCEL','$sysdatetime')");  
}
 
?>

<?php

closeConnection($conn);  

?>