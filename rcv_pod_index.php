
<?php 
  include 'header.php';

  $id = $conn->real_escape_string($_REQUEST['id']);

  $sql = "SELECT * FROM podmemo WHERE memono='$id'";
  $res = $conn->query($sql);
  $row = $res->fetch_assoc();
?>  
<link rel="stylesheet" href="vendor/jquery-ui.min.css" type="text/css" />     
<script type="text/javascript" src="vendor/jquery-ui.min.js"></script>  
<script src="vendor/jquery.dataTables.min.js"></script>
<style> 
.table-hover tbody tr:hover td,.table-hover tbody tr:hover th{background-color:#ffedda}.table td{vertical-align:middle!important;font-size:11px!important;color:#000;font-family:Verdana,Geneva,sans-serif;padding-top:4px;padding-right:4px;padding-bottom:4px;padding-left:10px}.table-bordered td{border:3px solid #e3e6f0}#user_data_info,#user_data_length{float:left}#user_data_filter,#user_data_paginate{float:right}#user_data1_info,#user_data1_length{float:left; display: none;}#user_data1_filter,#user_data1_paginate{float:right; display:  none;}.paginate_button{color:#000;float:left;padding:6px 12px;text-decoration:none;border:1px solid #ccc;cursor:pointer}.ellipsis{display:none}[type=search]{margin-right:10px; width: 250px; }.ui-autocomplete{z-index:2150000000!important}.container input{position:absolute;opacity:0;cursor:pointer;height:0;width:0}.checkmark{border-radius:2px;position:absolute;top:0;height:20px;width:20px;background-color:#fff;border:1px solid #000}.container:hover input~.checkmark{background-color:#fff}.container input:checked~.checkmark{background-color:#fff}.container input:disabled~.checkmark{background-color:#eaecf4}.checkmark:after{content:"";position:absolute;display:none}.container input:checked~.checkmark:after{display:block}.container .checkmark:after{left:6px;top:-1px;width:8px;height:16px;border:solid #000;border-width:0 3px 3px 0;-webkit-transform:rotate(45deg);-ms-transform:rotate(45deg);transform:rotate(45deg)}button:disabled,button[disabled]{border:1px solid #333!important;color:#333!important;cursor:no-drop} .table .thead-light th{text-align: center; font-size: 11px; color:#444; text-transform: uppercase; } .component{display: none;} 
	table {width: 100% !important;} table.table-bordered.dataTable td { white-space: nowrap; overflow: hidden; text-overflow:ellipsis;  }

	/* This css is for normalizing styles. You can skip this. */
*, *:before, *:after {
  -webkit-box-sizing: border-box;
  -moz-box-sizing: border-box;
  box-sizing: border-box;
  margin: 0;
  padding: 0;
}


td:nth-child(1){
	padding: 0px !important;
	width: 120px;
}

.new {
/*  padding: 50px;
*/}

.form-group {
  display: block;
  margin-bottom: 15px;
}

.form-group input {
  padding: 0;
  height: initial;
  width: initial;
  margin-bottom: 0;
  display: none;
  cursor: pointer;
}

.form-group label {
  position: relative;
  cursor: pointer;
}

.form-group label:before {
  content:'';
  -webkit-appearance: none;
  background-color: transparent;
  border: 2px solid #004973;
  background-color: #FBFBFB;
  box-shadow: 0 1px 2px rgba(0, 0, 0, 0.05), inset 0px -15px 10px -12px rgba(0, 0, 0, 0.05);
  padding: 8px;
  display: inline-block;
  position: relative;
  vertical-align: middle;
  cursor: pointer;
  margin-right: 5px;
}

.form-group input:checked + label:after {
  content: '';
  display: block;
  position: absolute;
  top: 2px;
  left: 7px;
  width: 6px;
  height: 13px;
  border: solid #004973;
  border-width: 0 2px 2px 0;
  transform: rotate(45deg);
}

input{
	text-transform: uppercase;
}
 
#appenddiv, #appenddiv2 {
    display: block; 
    position:relative
} 
.ui-autocomplete {
    position: absolute;
}
/*.dataTables_info{
  display: none;
}*/
</style>

<div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="memberModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content"> 
            <div class="dash" id="modaldetail"> 
            </div> 
        </div>
    </div>
</div>

<div id="content-wrapper" class="d-flex flex-column"> 
<div id="content">
<!-- <div class="mb-4"></div>
 -->
<div id="updatereq_status"></div> 
 <div class="container-fluid"> 
<div class="row"> 

<div class="col-md-12" >
<div class="card shadow mb-4"> 
<div class="card-header">
 <?php
     $narration = "NA";
  if($row["sentby"]=="COURIER"){
    $narration = "Courier Name: ".$row['couriername']." / Docket No: ".$row['docketno'];
  } else if($row["sentby"]=="TRUCK"){
    $narration = "Truck No: ".$row['truckno']." / Driver Name: ".$row['drivername']." / Driver Mobile: ".$row['drivermobile'];
  } else if($row["sentby"]=="PERSON"){
    $narration = "Person Name: ".$row['contactname']." / Person Mobile: ".$row['contactmobile'];
  } else if($row["sentby"]=="OTHERS"){
    $narration = "Others: ".$row['narration'];
  }
  ?>
<h6 class="m-0 font-weight-bold text-dark" style="top: 6px; float: left !important; font-family: Verdana, Geneva, sans-serif; font-weight: normal; text-transform: none;"> Intermemo No: <?= $id; ?> / From Station: <?php echo $row['branch']; ?> 
 </h6>
 <br>
 <p style="padding-top: 5px; margin-bottom: 15px;"> <?php echo $row["sentby"]; ?> (<?php echo $narration; ?>) </p>

</div>
<div class="card-body table-responsive" style="padding-top: 10px;">



<table id="user_data" class="table table-bordered table-hover" style="">
      <thead class="thead-light">
        <tr>

        <th style=" text-align: center;"> Sno </th>
        <th style=" text-align: center;"> FM / Bilty </th>
        <th style=" text-align: center;"> LR_No </th>
        <th style=" text-align: center;"> LR_Date </th>
        <th style=" text-align: center;"> Truck_No </th>
        <th style=" text-align: center;"> From_Station </th>
        <th style=" text-align: center;"> To_Station </th> 
        <th style=" text-align: center;"> Consignor </th> 
        <th style=" text-align: center;"> POD_Copy </th>
        <th style=" text-align: center;"> POD_Date </th>
        <th style=" text-align: center;"> #</th>
        <th style=" text-align: center;"> # </th>

        </tr>
      </thead>
  
  </table>

  <div > &nbsp; </div>


    <table style="margin-top: 50px;" id="user_data1" class="table table-bordered table-hover" >
      <thead class="thead-light">
        <tr>

        <th style=" text-align: center;"> ID </th>
        <th style=" text-align: center;"> Shipment_No </th>
        <th style=" text-align: center;"> Shipment_Date </th> 
        <th style=" text-align: center;"> Item </th> 
        <th style=" text-align: center;"> # </th> 
        <th style=" text-align: center;"> # </th>

        </tr>
      </thead>
  
  </table>

</div>
</div>
</div> 
</div>
</div> 
</div>
</div>

<div id="response"></div>

<div id="dataModal" class="modal fade">  
      <div class="modal-dialog">  
           <div class="modal-content" id="employee_detail">   
           </div>  
      </div>  
</div>    

<script type="text/javascript">
jQuery( document ).ready(function() {

$('#loadicon').show(); 
var table = jQuery('#user_data').dataTable({
     "lengthMenu": [ [7, 500, 1000, -1], [7, 500, 1000, "All"] ], 
     "bProcessing": true,
     "sAjaxSource": "rcv_pod_fetch.php?id=<?= $id ?>",
      "bPaginate": true,
      "sPaginationType":"full_numbers",
      "iDisplayLength": 7,
      //"order": [[ 8, "desc" ]],
      "language": {
            "loadingRecords": "&nbsp;",
            "processing": "<center> <font color=brown> Please wait while Loading </font> <img src='https://www.mypsdtohtml.com/_ui/images/loading.gif' height=20> </center>"
        },

      "columnDefs":[
      {
        "targets":[0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11],
        "orderable":false,
      },
      ],
    //   "aoColumns": [
    //     { mData: '0' },
    //     { mData: '1' } ,
    //     { mData: '2' },
    //     { mData: '3' },
    //     { mData: '4' },
    //     { mData: '5' },
    //     { mData: '6' },
    //     { mData: '7' },
    //     { mData: '9' },
    //     { mData: '10' },
    //     { mData: '11' }
    // ],
    "initComplete": function( settings, json ) {
    $('#loadicon').hide();
    }
});  

// $('#loadicon').show(); 
var table = jQuery('#user_data1').dataTable({
     "lengthMenu": [ [10, 500, 1000, -1], [10, 500, 1000, "All"] ], 
     "bProcessing": true,
     "sAjaxSource": "rcv_ship_fetch.php?id=<?= $id ?>",
      "bPaginate": false,
      "sPaginationType":"full_numbers",
      "iDisplayLength": 10,
      //"order": [[ 8, "desc" ]],
      "columnDefs":[
      {
        "targets":[0, 1, 2, 3, 4, 5],
        "orderable":false,
      },
      ],
      "aoColumns": [
        { mData: '0' },
        { mData: '1' } ,
        { mData: '2' },
        { mData: '3' },
        { mData: '4' },
        { mData: '5' }
    ],
    "initComplete": function( settings, json ) {
    // $('#loadicon').hide();
    }
});  
      

}); 
  
  function action(val,stat,memo){
        $('#loadicon').show();
        $.ajax({
            type: 'POST',
            url: 'rcv_pod_action.php',
            data: {id: val, act: stat, memo: memo},
            success: function(data){ 
            // alert(data);
            $('#response').html(data);
            $('#user_data').DataTable().ajax.reload(null, false); 
            // $('#user_data1').DataTable().ajax.reload();
            $('#loadicon').hide(); 
            }
        }); 
  } 

  function action2(val,stat,memo){
      $('#loadicon').show();
      $.ajax({
          type: 'POST',
          url: 'rcv_ship_action.php',
          data: {id: val, act: stat, memo: memo},
          success: function(data){ 
          // alert(data);
          $('#response').html(data);
          // $('#user_data').DataTable().ajax.reload();
          $('#user_data1').DataTable().ajax.reload();
          $('#loadicon').hide(); 
          }
      }); 
  }

</script>
<script type="text/javascript">
$(document).ready(function() { 
    var table = $('#user_data').DataTable(); 
    var table = $('#user_data1').DataTable(); 
} );
</script>   
<?php 
  include 'footer.php';
?>  
 