<?php
require('connect.php'); 
$id = $conn->real_escape_string($_REQUEST['id']);
?>
 
<style type="text/css"> 
.modal-backdrop
{
    opacity:0.5 !important;
}
</style>
<form method="post" action="" id="updatereq" role="form" autocomplete="off">
	<div class="modal-body">
<p style="color: #444;"> UPDATE DOCKET NO. <button type="button" class="close" data-dismiss="modal"> &times; </button> <p style="border-bottom: 1px solid #ccc;"></p>
		</p>

 			<div class="row">
                    <div class="form-group col-md-12">
						<input type="text" oninput="this.value=this.value.replace(/[^a-z A-Z 0-9&-]/,'')" style="text-transform: uppercase;" class="form-control" id="name" placeholder=" " name="name" required>
						<input type="hidden" value="<?php echo $id; ?>" name="id">
                    </div>  
				</div>   
			</div>
		<div class="modal-footer">
			<button type="button" id="hidemodal" class="btn btn-warning" data-dismiss="modal">CLOSE</button>
			<input type="submit" id="" class="btn btn-primary" name="submit" value="SAVE" />
		</div>
	</form> 
 
<?php
// closeConnection($conn);
?> 