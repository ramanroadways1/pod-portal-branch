<?php

  require('connect.php');
  
  // update code to get docket no from the intermemo side not lr side

	$connection = new PDO('mysql:host='.$host.';dbname='.$db_name.';', $username, $password );
	$statement = $connection->prepare("select shipment.*, GROUP_CONCAT(shipment_item.item) as item from shipment
RIGHT join shipment_item on shipment_item.shipno = shipment.shipno
where user='$branchuser' and collect='0' GROUP by shipno order by id desc");
	$statement->execute();
	$result = $statement->fetchAll();
	$count = $statement->rowCount();
	$data = array();
$sno = 0;
foreach($result as $row)
{ 
  $sno++;
	$sub_array = array(); 
  
  
  $btn= "<center> 

  <button onclick='window.open(\"shipment_print.php?id=".$row['shipno']."\", \"_blank\");' class='btn btn-sm btn-success' style='margin-left: 10px; color: #fff; letter-spacing: 1px;'> <i class='fa fa-print'></i> PRINT  </button>

  </center>"; 

  $sub_array[] = $btn; 
	$sub_array[] = $row["shipno"];
  $sub_array[] = $row["source"]; 
  $sub_array[] = $row["destination"]; 
  $sub_array[] = date('d/m/Y', strtotime($row['dispatchdate'])); 
  $sub_array[] = $row["dispatchvia"]; 
  if($row["memono"]=="NA"){
  $sub_array[] = "Direct Dispatch"; 

  }else {
  $sub_array[] = $row["memono"]; 
  }
  $sub_array[] = $row["item"]; 

 
	$data[] = $sub_array; 
} 

$results = array(
	"sEcho" => 1,
    "iTotalRecords" => $count,
    "iTotalDisplayRecords" => $count,
    "aaData"=>$data);

echo json_encode($results); 
exit
?>
 