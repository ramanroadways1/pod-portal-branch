<?php

  require('connect.php');
 
   // $p = $branchuser;
   // $f = $conn_rrpl->real_escape_string($_REQUEST['f']);
//    $t = $conn_rrpl->real_escape_string($_REQUEST['t']);

// $f = date("Y-m-d",$f);
// $t = date("Y-m-d",$t);

$connection = new PDO('mysql:host='.$host.';dbname='.$db_name.';', $username, $password );
$statement = $connection->prepare("
    SELECT r.*,

       if(m.lrdate is NULL,l.date,m.lrdate) as lrdate, 

    if(m.billing_party is NULL,l.consignor,m.billing_party) as consignor,

   if(m.tno is NULL,l.truck_no,m.tno) as truck_no ,

   datediff(curdate(),exdate) as diff FROM rcv_pod r left join lr_sample l on l.lrno=r.lrno left join mkt_bilty m on m.bilty_no = r.lrno where r.fromstation='$branchuser' and nullify='0' and self='0' and billing!='1' order by pod_date asc");
$statement->execute();
$result = $statement->fetchAll();
$count = $statement->rowCount();
$data = array();

$sno=0;
foreach($result as $row)
{ 
  $sno = $sno+1;
	$sub_array = array(); 
 

 //  $btn= "<center> <div class='form-group' style='margin:0px !important;'> <input name='mark[]' type='checkbox' id='".$row["id"]."' value='".$row["id"]."'> <label for='".$row["id"]."'>   </label> </div>   </center> "; 
 //  $sub_array[] = $btn; 
	$sub_array[] = "<center>".$sno."</center>";
  $sub_array[] = $row["veh_type"]; 
  $sub_array[] = $row["frno"]; 
  $sub_array[] = $row["truck_no"]; 
  $sub_array[] = $row["lrno"]; 
  $sub_array[] = date('d/m/Y', strtotime($row['lrdate']));
  $sub_array[] = date('d/m/Y', strtotime($row['pod_date']));
  $sub_array[] = date('d/m/Y', strtotime($row['exdate']));
 $pod_files1 = array(); 
$copy_no = 0;
foreach(explode(",",$row['pod_copy']) as $pod_copies)
{
  $copy_no++;
        
        if (strpos($pod_copies, 'pdf') !== false) {
        $file = 'PDF';
        } else {
        $file = 'IMAGE';
        }

    if($row['veh_type']=="MARKET"){
      $pod_files1[] = "<center><a href='https://rrpl.online/b5aY6EZzK52NA8F/$pod_copies' target='_blank'>$file: $copy_no</a></center>";
    } else {
      $pod_files1[] = "<a href='https://rrpl.online/diary/close_trip/$pod_copies' target='_blank'>$file: $copy_no</a>";
    }
 }
  $sub_array[] = implode(", ",$pod_files1);
    $sub_array[] = $row["diff"]." days"; 
  $sub_array[] = $row["consignor"]; 
  $sub_array[] = $row["fromstation"]; 
  if($row["tostation"]==NULL || $row["tostation"]==""){
      $sub_array[] = "<center> <font color=red> NA </font> </center>"; 

  }else {
      $sub_array[] = "<center> ".$row["tostation"]." </center>";  
  }

  $stat = "";
  // if($row['billing']=="" && $row['colset']=="-1"){
  // $stat = "<font color='red'>CANCEL</font>";
  // }else {
  // $stat = "NA";
  // }
  
  $sub_array[] = $stat; 

    
	$data[] = $sub_array;

} 

$results = array(
	"sEcho" => 1,
    "iTotalRecords" => $count,
    "iTotalDisplayRecords" => $count,
    "aaData"=>$data);

echo json_encode($results); 
exit
?>
 