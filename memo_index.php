<?php 
  require 'header.php';
?>   
<style> 
.table-hover tbody tr:hover td,.table-hover tbody tr:hover th{background-color:#ffedda;}.table td{vertical-align:middle!important;font-size:11px!important;color:#000;font-family:Verdana,Geneva,sans-serif;padding-top:4px;padding-right:4px;padding-bottom:4px;padding-left:10px}.table-bordered td{border:3px solid #e3e6f0}#user_data_info,#user_data_length{float:left}#user_data_filter,#user_data_paginate{float:right}.paginate_button{color:#000;float:left;padding:6px 12px;text-decoration:none;border:1px solid #ccc;cursor:pointer}.ellipsis{display:none}[type=search]{margin-right:10px; width: 250px; }.ui-autocomplete{z-index:2150000000!important}.container input{position:absolute;opacity:0;cursor:pointer;height:0;width:0}.checkmark{border-radius:2px;position:absolute;top:0;height:20px;width:20px;background-color:#fff;border:1px solid #000}.container:hover input~.checkmark{background-color:#fff}.container input:checked~.checkmark{background-color:#fff}.container input:disabled~.checkmark{background-color:#eaecf4}.checkmark:after{content:"";position:absolute;display:none}.container input:checked~.checkmark:after{display:block}.container .checkmark:after{left:6px;top:-1px;width:8px;height:16px;border:solid #000;border-width:0 3px 3px 0;-webkit-transform:rotate(45deg);-ms-transform:rotate(45deg);transform:rotate(45deg)}button:disabled,button[disabled]{border:1px solid #333!important;color:#333!important;cursor:no-drop} .table .thead-light th{text-align: center; font-size: 11px; color:#444; text-transform: uppercase; } .component{display: none;} 
	table {width: 100% !important;} table.table-bordered.dataTable td { white-space: nowrap; overflow: hidden; text-overflow:ellipsis;  }

  table tbody tr td {text-align: center;}

</style>
  

<div id="response"></div> 


 <div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="memberModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-md">
        <div class="modal-content"> 
            <div class="dash" id="modaldetail"> 
            </div> 
        </div>
    </div>
 </div>


<div class="row"> 
<div class="col-md-12"> <h3 style="float: left; margin-top: 10px;">  Outstanding Inter Memo (जो इंटर मेमो अभी तक कलेक्ट करना पेंडिंग हो) </h3> </div>

<div class="col-md-12" >  
<div class="card shadow mb-4"> 
<div class="card-body">
<div class="table-responsive">
  
  <table id="user_data" class="table table-bordered table-hover " style="">
      <thead class="thead-light">
        <tr>  
        <th> # </th>  
        <th> InterMemo  </th>
        <th> MemoDate </th>
        <th> From Station </th>
        <th> Destination </th>
        <th> Dispatch Date </th>
        <th> Sended_By </th>
        <th> Narration </th>
        </tr>
      </thead>
  
  </table>
</div>

  
</div>  
</div>
</div> 
</div>  
 
<script type="text/javascript"> 

jQuery( document ).ready(function() {

$('#loadicon').show(); 
var table = jQuery('#user_data').dataTable({
     "lengthMenu": [ [7, 500, 1000, -1], [7, 500, 1000, "All"] ], 
     "bProcessing": true,
     "sAjaxSource": "memo_fetch.php",
      "bPaginate": true,
      "sPaginationType":"full_numbers",
      "iDisplayLength": 7,
      //"order": [[ 8, "desc" ]],
      "columnDefs":[
      {
        "targets":[0, 1, 2, 3, 4, 5, 6,7],
        "orderable":false,
      },
      ],
      "aoColumns": [
    ],
    "initComplete": function( settings, json ) {
    $('#loadicon').hide();
    }
});  
 
});  

    function update(val){
    $('#loadicon').show(); 
           var id = val;
           $.ajax({
                url:"doc_update.php",  
                method:"post",  
                data:{id:id},  
                success:function(data){  
                     $('#modaldetail').html(data);  
                     $('#exampleModal').modal("show");  
                     $('#loadicon').hide(); 
                }
           });
    }


    function view(val){
    $('#loadicon').show(); 
           var id = val;
           $.ajax({
                url:"doc_view.php",  
                method:"post",  
                data:{id:id},  
                success:function(data){  
                     $('#modaldetail').html(data);  
                     $('#exampleModal').modal("show");  
                     $('#loadicon').hide(); 
                }
           });
    }

    $(document).on('submit', '#updatereq', function()
    {
    $('#loadicon').show(); 
      var data = $(this).serialize(); 
      $.ajax({
        type : 'POST',
        url  : 'doc_insert.php',
        data : data,
        success: function(data) {  
        document.getElementById("hidemodal").click();  
        $('#response').html(data);  
        $('#user_data').DataTable().ajax.reload();  
        $('#loadicon').hide(); 
        }
      });
      return false;
    });
</script>
<script type="text/javascript">
$(document).ready(function() { 
    var table = $('#user_data').DataTable(); 
} );
</script>   
<?php 
  include 'footer.php';
?>  
 