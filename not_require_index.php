<?php 
  require 'header.php';
  $id = "VISALPUR";

  // $sql = mysqli_query($conn,"SELECT * FROM billing_party WHERE id='$id'");
  // if(mysqli_num_rows($sql)==0)
  // {
  //   exit();
  // } 
  // $row=mysqli_fetch_array($sql);
 
?>   
<style> 
  #user_data1_info,#user_data1_length{float:left}#user_data1_filter,#user_data1_paginate{float:right}
  #user_data2_info,#user_data2_length{float:left}#user_data2_filter,#user_data2_paginate{float:right}
.table-hover tbody tr:hover td,.table-hover tbody tr:hover th{background-color:#ffedda}.table td{vertical-align:middle!important;font-size:11px!important;color:#000;font-family:Verdana,Geneva,sans-serif;padding-top:4px;padding-right:4px;padding-bottom:4px;padding-left:10px}.table-bordered td{border:3px solid #e3e6f0}#user_data_info,#user_data_length{float:left}#user_data_filter,#user_data_paginate{float:right}.paginate_button{color:#000;float:left;padding:6px 12px;text-decoration:none;border:1px solid #ccc;cursor:pointer}.ellipsis{display:none}[type=search]{margin-right:10px; width: 250px; }.ui-autocomplete{z-index:2150000000!important}.container input{position:absolute;opacity:0;cursor:pointer;height:0;width:0}.checkmark{border-radius:2px;position:absolute;top:0;height:20px;width:20px;background-color:#fff;border:1px solid #000}.container:hover input~.checkmark{background-color:#fff}.container input:checked~.checkmark{background-color:#fff}.container input:disabled~.checkmark{background-color:#eaecf4}.checkmark:after{content:"";position:absolute;display:none}.container input:checked~.checkmark:after{display:block}.container .checkmark:after{left:6px;top:-1px;width:8px;height:16px;border:solid #000;border-width:0 3px 3px 0;-webkit-transform:rotate(45deg);-ms-transform:rotate(45deg);transform:rotate(45deg)}button:disabled,button[disabled]{border:1px solid #333!important;color:#333!important;cursor:no-drop} .table .thead-light th{text-align: center; font-size: 11px; color:#444; text-transform: uppercase; } .component{display: none;} 
  table {width: 100% !important;} table.table-bordered.dataTable td { white-space: nowrap; overflow: hidden; text-overflow:ellipsis;  }

  /* This css is for normalizing styles. You can skip this. */
*, *:before, *:after {
  -webkit-box-sizing: border-box;
  -moz-box-sizing: border-box;
  box-sizing: border-box;
  margin: 0;
  padding: 0;
}


td:nth-child(1){
  padding: 0px !important;
  width: 120px;
}

.new {
/*  padding: 50px;
*/}

.form-group {
  display: block;
  margin-bottom: 15px;
}

.form-group input {
  padding: 0;
  height: initial;
  width: initial;
  margin-bottom: 0;
  display: none;
  cursor: pointer;
}

.form-group label {
  position: relative;
  cursor: pointer;
}

.form-group label:before {
  content:'';
  -webkit-appearance: none;
  background-color: transparent;
  border: 2px solid #004973;
  background-color: #FBFBFB;
  box-shadow: 0 1px 2px rgba(0, 0, 0, 0.05), inset 0px -15px 10px -12px rgba(0, 0, 0, 0.05);
  padding: 8px;
  display: inline-block;
  position: relative;
  vertical-align: middle;
  cursor: pointer;
  margin-right: 5px;
}

.form-group input:checked + label:after {
  content: '';
  display: block;
  position: absolute;
  top: 2px;
  left: 7px;
  width: 6px;
  height: 13px;
  border: solid #004973;
  border-width: 0 2px 2px 0;
  transform: rotate(45deg);
}

input{
  text-transform: uppercase;
}
 
#appenddiv, #appenddiv2 {
    display: block; 
    position:relative
} 
.ui-autocomplete {
    position: absolute;
}

.card{
  border-radius: 0px;
}

table{
  /*margin: 0px !important;*/
}
</style>
  

<div id="response"></div> 

<form id="save" action="" method="post" autocomplete="off">
<div class="row"> 

<div class="col-md-12" >
<div class="card shadow mb-4"> 
<div class="card-header">
 
<h6 class="m-0 font-weight-bold text-dark" style="text-align: center; font-size: 17px; text-transform: none; padding-top: 10px;  text-transform: uppercase;"> POD NOT REQUIRE </h6> 
</div>
<div class="card-body" style="padding-top: 0px;">
<div class="table-responsive"> 
  <table id="user_data" class="table table-bordered table-hover" style="">
      <thead class="thead-light">
        <tr>  
        <th> Dispatch </th>  
        <th > Sno </th>
        <th > FMNO </th>
        <th > LRNo </th>
        <th > LR Date </th>
        <th > Truck No </th>
        <th > From Station </th>
        <th > To Station </th>
        <th > Consignor </th>
        <th > POD Copy </th>
        <th > POD Date </th> 
        <th > Status </th> 
        <th > Update </th> 
        </tr>
      </thead> 
  </table> 
</div> 

<!-- <div class="table-responsive" style="margin-top: 30px;"> 
  <h6 style="text-align: center; margin:0px;">Market Bilty</h6>
  <table id="user_data1" class="table table-bordered table-hover" style="">
      <thead class="thead-light">
        <tr>  
        <th> Dispatch </th>  
        <th > Sno </th>
        <th > Bilty No </th>
        <th > LR No </th>
        <th > Bilty Date </th>
        <th > Truck No </th>
        <th > From Station </th>
        <th > To Station </th>
        <th > Billing Party </th>
        <th > POD Copy </th>
        <th > POD Date </th> 
        <th > Status </th> 
        <th > Update </th> 
        </tr>
      </thead> 
  </table> 
</div>
 
<div class="table-responsive" style="margin-top: 30px;"> 
  <h6 style="text-align: center; margin:0px;">TRIP FINAL HISAB</h6>
  <table id="user_data2" class="table table-bordered table-hover" style="">
      <thead class="thead-light">
        <tr>  
        <th > Dispatch </th>  
        <th > Sno </th>
        <th > Trip No </th>
        <th > Truck No </th>
        <th > Opening Date </th>
        <th > Closing Date </th>  
        <th > Opening Branch </th>
        <th > Closing Branch </th>
        <th > Status </th> 
        <th > Update </th> 
        </tr>
      </thead> 
  </table> 
</div>
 -->
</div>
<div class="col-md-12" style="padding-bottom: 12px;">
  
  <div class="row"> 
  <div class="col-md-4">
    
  </div> 
  <div class="col-md-2">
  <label style="text-transform: uppercase; color: #444;"> DISPATCH TO </label>
  <select name="dispatchto" class="form-control" required="" onChange="dispatch(this);">
    <option value=""> -- select -- </option>
    <!-- <option value="default" > <?php echo $id; ?> </option> -->
    <!-- <option value="outmemono"> Outstanding Intermemo </option> -->
    <option value="otherbranch"> Warehouse Branch </option>
  </select>
  </div> 


  <div class="col-md-4">
          <label style="text-transform: uppercase; color: #444;"> Outstanding Intermemo </label>
          <select class="form-control" name="outmemono" id="outmemono" disabled="">
            <option value=""> --  select -- </option>
             <?php
             $sql = "select * from podmemo where branch='$branchuser' and status='0' order by id desc";
             $res = $conn->query($sql);
             while($row = $res->fetch_assoc()){

                $narration = "NA";
              if($row["sentby"]=="COURIER"){
                $narration = "Courier Name: ".$row['couriername']." / Docket no: ".$row['docketno'];
              } else if($row["sentby"]=="TRUCK"){
                $narration = "Truck No: ".$row['truckno']." / Driver Name: ".$row['drivername']." / Driver Mobile: ".$row['drivermobile'];
              } else if($row["sentby"]=="PERSON"){
                $narration = "Person Name: ".$row['contactname']." / Person Mobile: ".$row['contactmobile'];
              } else if($row["sentby"]=="OTHERS"){
                $narration = "Others: ".$row['narration'];
              }

              echo "<option value='$row[memono]'> $row[memono] (".ucfirst(strtolower($row['branch']))." to ".ucfirst(strtolower($row['bill_branch'])).") Via $narration </option>";
             }
             ?>
          </select>
  </div>

  <div class="col-md-2">
    <label style="text-transform: uppercase; color: #444;"> Branch </label>
    <select class="form-control" name="otherbranch" id="otherbranch" disabled="">
            <option value=""> --  select -- </option>
             <?php
               $sql = "select * from user where role='2' and username='VISALPUR' order by username asc";
               $res = $conn->query($sql);
               while($row = $res->fetch_assoc()){  

                echo "<option value='$row[username]'> $row[username] </option>";

               }
             ?>
          </select>
  </div>


  </div>
</div>

<div class="col-md-12" style="padding-bottom: 12px;">
  <div class="row">

 
    <div class="col-md-2 ">
      <label style="text-transform: uppercase; color: #444;"> SEND BY </label>
      <select id="sendby" class="form-control" name="sendby"  onChange="updt(this);" required="required">
        <option value=""> -- select -- </option>
        <option value="COURIER" disabled=""> COURIER </option>
        <option value="TRUCK"> TRUCK  </option> 
        <option value="PERSON" disabled=""> PERSON </option> 
        <option value="OTHERS" disabled=""> OTHERS </option> 
      </select>
    </div>
  <div class="col-md-2">
            <label style="text-transform: uppercase; color: #444;"> Date </label>
            <input oninput="this.value=this.value.replace(/[^a-z 0-9 A-Z.&-]/,'')" type="date" class="form-control" id="date" name="date" value="" required="" min="<?php echo date('Y-m-d',strtotime("-2 days")); ?>" max="<?php echo date('Y-m-d'); ?>" />
    </div>

 

    <div class="col-md-2" class="display1">
      <label style="text-transform: uppercase; color: #444;"> Courier  </label>
      <select id="val10" name="val10" class="form-control" disabled="" onChange="maruti(this);">
          <option value=""> -- select -- </option>
          <option value="Shree Maruti Courier">Shree Maruti Courier</option>
          <option value="others"> Others </option>        
      </select>
    </div>

    <div class="col-md-2" class="display1">
            <label style="text-transform: uppercase; color: #444;"> Courier Name </label>
            <input oninput="this.value=this.value.replace(/[^a-z 0-9 A-Z.&-]/,'')" type="text" class="form-control" id="val1" name="val1" value="" readonly="" />
    </div>



 <div class="col-md-2" class="display2" >
            <label style="text-transform: uppercase; color: #444;">Have Docket No ? </label>
            <select id="doc" name="doc" class="form-control" disabled="" onChange="updtdoc(this);"> 
              <option value=""> -- select -- </option>
              <option value="YES"> Yes </option>
              <option value="NO"> No </option>
            </select>
    </div> 

    <div class="col-md-2" class="display1">
            <label style="text-transform: uppercase; color: #444;"> Docket No </label>
            <input oninput="this.value=this.value.replace(/[^a-z 0-9 A-Z.&-]/,'')" type="text" class="form-control" id="val2" name="val2" value="" disabled=""/>
    </div>

 </div>
</div>
<div class="col-md-12" style="padding-bottom: 20px;">
  <div class="row">
    <div class="col-md-2" class="display2" >
            <label style="text-transform: uppercase; color: #444;"> Contact Name </label>
            <input oninput="this.value=this.value.replace(/[^a-z 0-9 A-Z.&-]/,'')" type="text" class="form-control" id="val3" name="val3" value="" disabled="" />
    </div> 
  <div class="col-md-2" class="display2">
            <label style="text-transform: uppercase; color: #444;"> Contact Mobile No </label>
            <input oninput="this.value=this.value.replace(/[^a-z 0-9 A-Z.&-]/,'')" type="text" class="form-control" id="val4" name="val4" value="" disabled="" />
    </div> 

<script type="text/javascript">
  $(function() {
  $("#val5").autocomplete({
  source: 'pod_view_truck.php',
  appendTo: '#appenddiv',
  select: function (event, ui) { 
         $('#val5').val(ui.item.value);   
         $('#val6').val(ui.item.dname);
         $('#val7').val(ui.item.dcode);      
         return false;
  },
  change: function (event, ui) {
  if(!ui.item){
  $(event.target).val("");
  Swal.fire({
  icon: 'error',
  title: 'Error !!!',
  text: 'Truck does not exists !'
  })
  $("#val6").val("");
  $("#val7").val("");
  $("#val5").val("");
  $("#val5").focus();
  }
  }, 
  focus: function (event, ui){
  return false;
  }
  });
  });
</script>

  <div class="col-md-2" class="display3" >
            <label style="text-transform: uppercase; color: #444;"> Truck No </label>
            <input oninput="this.value=this.value.replace(/[^a-z 0-9 A-Z.&-]/,'')" type="text" class="form-control" id="val5" name="val5" value="" disabled="" />
            <div id="appenddiv"></div>
    </div> 

    <div class="col-md-2" class="display3" >
            <label style="text-transform: uppercase; color: #444;"> Driver Name </label>
            <input oninput="this.value=this.value.replace(/[^a-z 0-9 A-Z.&-]/,'')" type="text" class="form-control" id="val6" name="val6" value="" disabled="" />
    </div>
    
    <div class="col-md-2" class="display3" >
            <label style="text-transform: uppercase; color: #444;"> Driver Mobile </label>
            <input oninput="this.value=this.value.replace(/[^a-z 0-9 A-Z.&-]/,'')" type="text" class="form-control" id="val7" name="val7" value="" disabled="" />
    </div>

  <input type="hidden" name="billbranch" oninput="this.value=this.value.replace(/[^a-z 0-9 A-Z.&-]/,'')" value="<?php echo $id; ?>">
  <input type="hidden" name="billparty" oninput="this.value=this.value.replace(/[^a-z 0-9 A-Z.&-]/,'')" value="0">
  <input type="hidden" name="billpartyid" oninput="this.value=this.value.replace(/[^a-z 0-9 A-Z.&-]/,'')" value="0">
  

    <div class="col-md-2" class="display4" >
            <label style="text-transform: uppercase; color: #444;"> NARRATION </label>
            <input oninput="this.value=this.value.replace(/[^a-z 0-9 A-Z.&-]/,'')" type="text" class="form-control" id="val8" name="val8" value="" disabled="" />
 
     </div>

  <div class="col-md-2 offset-md-10" class="display3" >
      <button style="margin-top: 20px; letter-spacing: 1px; width: 100%;" type="submit" class=' btn btn-success'> <i class='fa fa-truck '></i> <b> DISPATCH </b> </button>    
	</div>
  
  </div>
</div>


</div>
</div> 
</div> 
</form>
 
<script type="text/javascript">

  function editbilty(val){
     $('#loadicon').show(); 
     var card = val;  

     if (confirm('Are you sure you ?')) {
          $.ajax({  
          url:"not_require_mark.php",  
          method:"post",  
          data:{card:card},  
          success:function(data){  
            window.alert(data);
            $('#dataModal').modal("hide"); 
            $('#user_data').DataTable().ajax.reload();  
            $('#loadicon').hide(); 
          }  
          });
     } else {
          $('#loadicon').hide(); 
     }
  }

  function maruti(sel){

    if(sel.value == "others"){
      $("#val1").prop('required', true);
      $("#val1").prop('readonly', false);
      $("#val1").val('');
    } else {
      $("#val1").prop('required', false);
      $("#val1").prop('readonly', true);
      $("#val1").val('SHREE MARUTI COURIER');
    }

  }

  function updtdoc(sel){

    if(sel.value == "YES"){
      $("#val2").prop('required', true);
      $("#val2").prop('disabled', false);
    } else {
      $("#val2").prop('required', false);
      $("#val2").prop('disabled', true);
    }

  }

  function updt(sel) {
  
    if(sel.value == "COURIER")
    {
      $("#val10").prop('required',true);
      $("#val10").prop('disabled',false);
      $("#doc").prop('required',true);
      $("#doc").prop('disabled',false);      
    } else {
      $("#val10").prop('required',false);
      $("#val10").prop('disabled',true);
      $("#doc").prop('required',false);
      $("#doc").prop('disabled',true);
  }
    if(sel.value == "TRUCK")
    {
      $("#val5").prop('required',true);
      $("#val5").prop('disabled',false);
      $("#val6").prop('required',true);
      $("#val6").prop('disabled',false);   
      $("#val7").prop('required',true);
      $("#val7").prop('disabled',false);  
    } else {
      $("#val5").prop('required',false);
      $("#val5").prop('disabled',true);
      $("#val6").prop('required',false);
      $("#val6").prop('disabled',true);
      $("#val7").prop('required',false);
      $("#val7").prop('disabled',true);
    }
    if(sel.value == "PERSON")
    {
      $("#val3").prop('required',true);
      $("#val3").prop('disabled',false);
      $("#val4").prop('required',true);
      $("#val4").prop('disabled',false);    
    } else { 
      $("#val3").prop('required',false);
      $("#val3").prop('disabled',true);
      $("#val4").prop('required',false);
      $("#val4").prop('disabled',true);
    }
    if(sel.value == "OTHERS")
    {
      $("#val8").prop('required',true);
      $("#val8").prop('disabled',false);
    } else {
      $("#val8").prop('required',false);
      $("#val8").prop('disabled',true);
    }
  } 

  function dispatch(val){
    $('#sendby').prop('selectedIndex',0);
    $('#outmemono').prop('selectedIndex',0);
    $('#otherbranch').prop('selectedIndex',0);
    $('#val10').prop('selectedIndex',0);
    $('#doc').prop('selectedIndex',0);
    
    if(val.value == "otherbranch"){
      $("#otherbranch").prop('required',true);
      $("#otherbranch").prop('disabled',false); 
      $("#outmemono").prop('required',false);
      $("#outmemono").prop('disabled',true);
      $("#sendby").prop('required',true);
      $("#sendby").prop('disabled',false); 
      $("#date").prop('required',true);
      $("#date").prop('disabled',false); 

    } else if(val.value == "outmemono"){ 
      $("#outmemono").prop('required',true);
      $("#outmemono").prop('disabled',false);
      $("#otherbranch").prop('required',false);
      $("#otherbranch").prop('disabled',true);
      $("#sendby").prop('required',false);
      $("#sendby").prop('disabled',true);
      $("#date").prop('required',false);
      $("#date").prop('disabled',true); 
        $("#val1").prop('required',false);
        $("#val1").prop('readonly',true);
        $("#val2").prop('required',false);
        $("#val2").prop('disabled',true);
        $("#val3").prop('required',false);
        $("#val3").prop('disabled',true);
        $("#val4").prop('required',false);
        $("#val4").prop('disabled',true);
        $("#val5").prop('required',false);
        $("#val5").prop('disabled',true);
        $("#val6").prop('required',false);
        $("#val6").prop('disabled',true);
        $("#val7").prop('required',false);
        $("#val7").prop('disabled',true);
        $("#val8").prop('required',false);
        $("#val8").prop('disabled',true);
        $("#val10").prop('required',false);
        $("#val10").prop('disabled',true);
        $("#doc").prop('required',false);
        $("#doc").prop('disabled',true);

    } else {  
      $("#otherbranch").prop('required',false);
      $("#otherbranch").prop('disabled',true);
      $("#outmemono").prop('required',false);
      $("#outmemono").prop('disabled',true);
      $("#sendby").prop('required',true);
      $("#sendby").prop('disabled',false); 
      $("#date").prop('required',true);
      $("#date").prop('disabled',false); 
    }
  }


jQuery( document ).ready(function() {

$('#loadicon').show(); 
var table = jQuery('#user_data').dataTable({
     "lengthMenu": [ [10, 500, 1000, -1], [10, 500, 1000, "All"] ], 
     
    // "scrollY": 400,
    // "scrollX": true,

     "bProcessing": true,
     "sAjaxSource": "not_require_fetch.php?id=<?= $id ?>",
      "bPaginate": true,
      "sPaginationType":"full_numbers",
      "iDisplayLength": 10,
      //"order": [[ 8, "desc" ]],
      "columnDefs":[
      {
        "targets":[0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12],
        "orderable":false,
      },
      ],
    //   "aoColumns": [
    //     { mData: '0' },
    //     { mData: '1' } ,
    //     { mData: '2' },
    //     { mData: '3' },
    //     { mData: '4' },
    //     { mData: '5' },
    //     { mData: '6' },
    //     { mData: '7' },
    //     { mData: '8' },
    //     { mData: '9' },
    //     { mData: '10' },
    //     { mData: '11' },
    //     { mData: '12' }
    // ],
    "initComplete": function( settings, json ) {
    $('#loadicon').hide();
    }
});  

// $('#loadicon').show(); 
var table = jQuery('#user_data1').dataTable({
     "lengthMenu": [ [10, 500, 1000, -1], [10, 500, 1000, "All"] ], 

    // "scrollY": 300,
    // "scrollX": true,

     "bProcessing": true,
     "sAjaxSource": "pod_view_fetch2.php?id=<?= $id ?>",
      "bPaginate": true,
      "sPaginationType":"full_numbers",
      "iDisplayLength": 10,
      //"order": [[ 8, "desc" ]],
      "columnDefs":[
      {
        "targets":[0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12],
        "orderable":false,
      },
      ],
      //   "aoColumns": [
      //     { mData: '0' },
      //     { mData: '1' },
      //     { mData: '2' },
      //     { mData: '3' },
      //     { mData: '4' },
      //     { mData: '5' },
      //     { mData: '6' },
      //     { mData: '7' },
      //     { mData: '8' },
      //     { mData: '9' },
      //     { mData: '10' },
      //     { mData: '11' },
      //     { mData: '12' }
      // ],
    "initComplete": function( settings, json ) {
    // $('#loadicon').hide();
    }
});  

var table = jQuery('#user_data2').dataTable({
     "lengthMenu": [ [10, 500, 1000, -1], [10, 500, 1000, "All"] ], 
     "bProcessing": true,
     "sAjaxSource": "pod_view_fetch3.php?id=<?= $id ?>",
      "bPaginate": true,
      "sPaginationType":"full_numbers",
      "iDisplayLength": 10,
      "columnDefs":[
      {
        // "targets":[0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12],
        "orderable":false,
      },
      ],
    "initComplete": function( settings, json ) {
    }
}); 

}); 
  
   function podup(val1,val2){
        $('#loadicon').show();
        $.ajax({
            type: 'POST',
            url: 'pod_view_update.php',
            data: {val1: val1, val2: val2},
            success: function(data){  
              $('#response').html(data)
              $('#user_data').DataTable().ajax.reload();
              $('#user_data1').DataTable().ajax.reload();
              $('#loadicon').hide(); 
            }
        }); 
  }

  function action(val,stat){
        $('#loadicon').show();
        $.ajax({
            type: 'POST',
            url: 'rcv_Pod_action.php',
            data: {id: val, act: stat},
            success: function(data){ 
            // alert(data);
            $('#user_data').DataTable().ajax.reload();
            $('#user_data1').DataTable().ajax.reload();
            $('#loadicon').hide(); 
            }
        }); 
  } 

    $(document).on('submit', '#save', function()
    {  
    $('#loadicon').show(); 
      var data = $(this).serialize(); 
      $.ajax({  
        type : 'POST',
        url  : 'pod_view_save.php',
        data : data,
        success: function(data) {  
        // document.getElementById("hidemodal").click();  
        $('#response').html(data);  
        $('#user_data').DataTable().ajax.reload();  
        $('#user_data1').DataTable().ajax.reload();  
        $('#user_data2').DataTable().ajax.reload();  
        $('#loadicon').hide(); 
        $("#save")[0].reset();
 
        $("#otherbranch").prop('required',false);
        $("#otherbranch").prop('disabled',true);
        $("#outmemono").prop('required',false);
        $("#outmemono").prop('disabled',true);
        $("#sendby").prop('required',true);
        $("#sendby").prop('disabled',false); 
        $("#date").prop('required',true);
        $("#date").prop('disabled',false); 
        $("#val1").prop('required',false);
        $("#val1").prop('readonly',true);
        $("#val2").prop('required',false);
        $("#val2").prop('disabled',true);
        $("#val3").prop('required',false);
        $("#val3").prop('disabled',true);
        $("#val4").prop('required',false);
        $("#val4").prop('disabled',true);
        $("#val5").prop('required',false);
        $("#val5").prop('disabled',true);
        $("#val6").prop('required',false);
        $("#val6").prop('disabled',true);
        $("#val7").prop('required',false);
        $("#val7").prop('disabled',true);
        $("#val8").prop('required',false);
        $("#val8").prop('disabled',true);
        $("#val10").prop('required',false);
        $("#val10").prop('disabled',true);
        $("#doc").prop('required',false);
        $("#doc").prop('disabled',true);
        }
      });
      return false;
    });

</script>
<script type="text/javascript">
$(document).ready(function() { 
    var table = $('#user_data').DataTable(); 
} );
$(document).ready(function() { 
    var table = $('#user_data1').DataTable(); 
} );
</script>   
<?php 
  include 'footer.php';
?>  
 