<?php 
   require ('connect.php');
   $id = $conn->real_escape_string($_REQUEST['id']);
?>
<!DOCTYPE html>
<html lang="en">
<head>
	<title>Intermemo - <?= $id; ?></title>
<script type="text/javascript">
	window.onload = function() { window.print(); }
</script>
<div id="new" style="display:none;position: fixed; right: 0px; top: 0px; width: 100%;height: 100%; background-color:#FFF; z-index: 30001; opacity:1;">
<center>
	<img style="margin-top:150px" src="../load.gif" />
</center>
</div>

<style>
.table-bordered > tbody > tr > th {
     border:solid #000 !important;
    border-width:1px !important;
}

.table-bordered > tbody > tr > td {
      border:solid #000 !important;
    border-width:1px !important;
}

.form-control{
	border:1px solid #000;
	text-transform:uppercase;
}
</style>	

<style type="text/css">
@media print
{
body {
   /*zoom: 100%;*/
 }	
body * { visibility: hidden; }
#printpage * { visibility: visible; }
#printpage { position: absolute; top: 0; left: 0; }
}
</style>

</head>
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
 
	 
<body style="overflow-x: scroll !important;"  onclick="location.href='#';">

	 
<div id="printpage">
 <div class="container-fluid">
	<div class="row" style="font-family:Verdana">
		<div class="col-md-12">
			<center><img src="logo_header.png" width="600px"></center>
			<br>
		</div>
	 <!--  <div class="col-md-3">
		
	</div>		
	 <div class="col-md-6">
		<center><span class="" style="font-size:16px;letter-spacing:1px">INTERMEMO NO -  </span> </center>
	</div>		
	 <div class="col-md-3">
		<span style="font-weight:bold;font-size:12px;margin-right:10px;" class="pull-right">

		</span>
	</div> -->
 </div>
 <div  style="margin-bottom: 10px;">
 	
 </div>


<div class="row">
<div class="col-md-12" style="font-family: verdana;">
<table border="0" width="100%" style="font-size:13px;">
	<tr>
		<?php
			$sql = 'select * from shipment where shipno="'.$id.'"';
			$res = $conn->query($sql);
			$row = $res->fetch_assoc();

			$dispatchvia = $row['dispatchvia'];
			$dispatchType = $row['memono'];

		?>
		<td style="text-align: right;"> <label>Shipment No &nbsp;</label> </td>
		<td style="text-align: right;">  <?= $id; ?> </td>
 
		<td style="text-align: right;"> <label>Dispatch Branch &nbsp;</label>  </td>
		<td style="text-align: right;"> <?= $row['source']; ?> </td>
	</tr>
	<tr>	
		<td style="text-align: right;"> <label>Destination Branch &nbsp;</label> </td>
		<td style="text-align: right;"> <?= $row['destination']; ?> </td>

		<td style="text-align: right;"> <label>Dispatch Date &nbsp;</label> </td>
		<td style="text-align: right;"> <?= date('d/m/Y h:i:s', strtotime($row['dispatchdate'])); ?> </td>
	</tr>		

	 
</table>
<br />
<style type="text/css">
	.table-bordered>tbody>tr>td, .table-bordered>tbody>tr>th{
		padding-top: 15px;
		padding-bottom: 10px;
	}
</style>
<div class="row" id="data_table_div">
	<div class="col-md-12" style="font-family:Verdana;color:#000; font-size:11px;">
		<div style="overflow-x:auto;" class="table-responsive"><table class="table table-bordered" style="margin-top:0px;font-family:Verdana;font-size:11px">

<tr class=""> 
			  <th>Sno </th>
			 <th>Item </th>
			 <th>Remarks </th>
		</tr>
		<?php
$sql = $conn->query("select * from rrpl_database.shipment_item where shipno='$id'");
						$sno = 0;
			while($row=$sql->fetch_assoc()){
			$sno = $sno+1;
			
			echo "<tr>";
			echo "<td> ".$sno." </td>";
			echo "<td> ".$row['item']." </td>";
			echo "<td>  </td>";
			echo "</tr>";
			
			}
	 	?> 
		<?php

 		// 	$sql = $conn->query("SELECT r.id, m.bilty_no, m.plr, m.date as mdate, m.tamt, r.date, r.copy, r.dispatch, r.dispatch_time, 
			// r.collect, r.collect_time, r.branch FROM `mkt_bilty` m left join dairy.rcv_pod r on r.lrno = m.bilty_no where r.memono='$id'");
			// // $sno = 0;
			// while($row=$sql->fetch_assoc()){
			// $sno = $sno+1;
			// echo "<tr>";
			// echo "<td> ".$sno." </td>";
			// echo "<td> ".$row['bilty_no']." </td>";
			// echo "<td> ".$row['plr']." </td>";
			// echo "<td> ".$row['tamt']." </td>";
			// echo "<td> ".$row['mdate']." </td>";
			// echo "<td> ".$row['date']." </td>";
			// echo "<td> ".$row['branch']." </td>";
			// echo "<td> ".$row['dispatch_time']." </td>";
			// echo "<td> ".$row['collect_time']." </td>";
			// echo "</tr>";
			
			// }
	 	?>

			 <?php
			// $sql = 'select * from podmemo where memono="'.$id.'"';
			// $res = $conn->query($sql);
			// $row = $res->fetch_assoc();
		?>
	<tr>
 				<td colspan="16"><?php

if($dispatchType=='NA'){
	$dispatchType = 'Direct Dispatch';
}
		// 		    $narration = "NA";
  // if($row["sentby"]=="COURIER"){
  //   $narration = "Courier Name: ".$row['couriername']." / Docket No: ".$row['docketno'];
  // } else if($row["sentby"]=="TRUCK"){
  //   $narration = "Truck No: ".$row['truckno']." / Driver Name: ".$row['drivername']." / Driver Mobile: ".$row['drivermobile'];
  // } else if($row["sentby"]=="PERSON"){
  //   $narration = "Person Name: ".$row['contactname']." / Person Mobile: ".$row['contactmobile'];
  // } else if($row["sentby"]=="OTHERS"){
  //   $narration = "Others: ".$row['narration'];
  // }
echo "<b>Intermemo/DD : </b> ".$dispatchType;

echo "  <b>& Dispatch via : </b> ".$dispatchvia;
?></td>
		</tr>								
</table>	
</div>
	</div>
</div>

<div class="row">
	<div class="col-md-12" style="font-family: Verdana; color:#000; font-size:12px;">
	</div>
</div>
  
        </div></div>
		</div> 
	
		 
</div>
	
</body>
</html>