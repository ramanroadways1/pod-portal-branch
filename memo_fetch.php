<?php

  require('connect.php');
  
  // update code to get docket no from the intermemo side not lr side

	$connection = new PDO('mysql:host='.$host.';dbname='.$db_name.';', $username, $password );
	$statement = $connection->prepare("select * from podmemo where branch='$branchuser' and status='0' order by id desc");
	$statement->execute();
	$result = $statement->fetchAll();
	$count = $statement->rowCount();
	$data = array();

foreach($result as $row)
{ 
	$sub_array = array(); 
  
  $btn= "<center><button onclick='view(\"".$row['memono']."\")' class='btn btn-sm btn-primary' style='margin-left: 10px; color: #fff; letter-spacing: 1px;'> <i class='fa fa-list'></i> VIEW  </button>

  <button onclick='window.open(\"reports_print.php?id=".$row['memono']."\", \"_blank\");' class='btn btn-sm btn-success' style='margin-left: 10px; color: #fff; letter-spacing: 1px;'> <i class='fa fa-print'></i> PRINT  </button>

  </center>"; 
  $sub_array[] = $btn; 
	$sub_array[] = $row["memono"];
  $sub_array[] = date('d/m/Y', strtotime($row['memodate'])); ; 
  $sub_array[] = $row["branch"]; 
  $sub_array[] = $row["bill_branch"]; 
  $sub_array[] = date('d/m/Y', strtotime($row['dispatchdate']));   
  $sub_array[] = $row["sentby"]; 

    $narration = "NA";
  if($row["sentby"]=="COURIER"){
    $narration = "Courier Name: ".$row['couriername']." / Docket no: ".$row['docketno'];
  } else if($row["sentby"]=="TRUCK"){
    $narration = "Truck No: ".$row['truckno']." / Driver Name: ".$row['drivername']." / Driver Mobile: ".$row['drivermobile'];
  } else if($row["sentby"]=="PERSON"){
    $narration = "Person Name: ".$row['contactname']." / Person Mobile: ".$row['contactmobile'];
  } else if($row["sentby"]=="OTHERS"){
    $narration = "Others: ".$row['narration'];
  }
 
  $sub_array[] = $narration; 
	$data[] = $sub_array; 
} 

$results = array(
	"sEcho" => 1,
    "iTotalRecords" => $count,
    "iTotalDisplayRecords" => $count,
    "aaData"=>$data);

echo json_encode($results); 
exit
?>
 