<?php
   	require ('connect.php');
  	$id = $conn->real_escape_string($_REQUEST['id']);

	$sql  = "select * from rrpl_database.zoop_rc where rc_regn_no='$id' order by id desc limit 1";
	if ($conn->query($sql) === FALSE) {
			$content = mysqli_error($conn);
			$content = preg_replace("/[^0-9a-zA-Z_: ]/", "", $content);  
			echo "<script>Swal.fire({
			icon: 'error',
			title: 'SQL Error !!!',
			text: '$content'
			})</script>";
			exit();
	}
	$row = $conn->query($sql)->fetch_assoc();


	// $sql  = "select name from rrpl_database.emp_attendance where code='$row[branch_user]'";
	// if ($conn->query($sql) === FALSE) {
	// 		$content = mysqli_error($conn);
	// 		$content = preg_replace("/[^0-9a-zA-Z_: ]/", "", $content);  
	// 		echo "<script>Swal.fire({
	// 		icon: 'error',
	// 		title: 'SQL Error !!!',
	// 		text: '$content'
	// 		})</script>";
	// 		exit();
	// }
	// $emp = $conn->query($sql)->fetch_assoc();
	// $branch_username = $emp['name'];
?>
<!DOCTYPE html>
<html lang="en">
<head>
<title>Registration No: <?php echo $id; ?></title>
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
<script src="https://cdn.jsdelivr.net/npm/sweetalert2@9"></script>

  <div id="loadicon" style="display:none; position: fixed; right: 0px; top: 0px; width: 100%;height: 100%; background-color:#ffffff; z-index: 30001; opacity:0.8; cursor: wait;">
  <center><img src="./assets/loader.gif" style="margin-top:50px;" /> </center>
  </div>
<!-- <div id="window_loadicon" style="position: fixed; right: 0px; top: 0px; width: 100%;height: 100%; background-color:#FFF; z-index: 30001; opacity:.98; cursor: wait">
	<center><img style="margin-top:100px" src="https://rrpl.online/b5aY6EZzK52NA8F/load.gif" /><br><b>Please wait ...</b></center>
</div>	 -->

<style>
table td[class*=col-], table th[class*=col-]{
	font-weight: bold !important;
	color: black;
	line-height: 20px;
}
table td[class*=col-] label, table th[class*=col-] label{ 
	font-weight: normal !important;
	color: black;
}

</style>	

<style type="text/css">
@media print
{

body {
   zoom:75%;
 }	
body * { visibility: hidden; }
.container-fluid * { visibility: visible; }
.container-fluid { position: absolute; top: 0; left: 0; }
 

.col-md-1, .col-md-2, .col-md-3, .col-md-4, .col-md-5, .col-md-6, .col-md-7, .col-md-8, .col-md-9, .col-md-10, .col-md-11, .col-md-12 {
  float: left;
}
.col-md-12 {
  width: 100%;
}
.col-md-11 {
  width: 91.66666666666666%;
}
.col-md-10 {
  width: 83.33333333333334%;
}
.col-md-9 {
  width: 75%;
}
.col-md-8 {
  width: 66.66666666666666%;
}
.col-md-7 {
  width: 58.333333333333336%;
}
.col-md-6 {
  width: 50%;
}
.col-md-5 {
  width: 41.66666666666667%;
}
.col-md-4 {
  width: 33.33333333333333%;
 }
 .col-md-3 {
   width: 25%;
 }
 .col-md-2 {
   width: 16.666666666666664%;
 }
 .col-md-1 {
  width: 8.333333333333332%;
 }

 	td {
  	 font-weight: bold !important;
  	 font-size: 14px !important;
  	 	line-height: 25px !important;

 	}
 	label {
  	 font-weight: normal !important;
  	 font-size: 12px !important;
	}	

}

</style>
<script type="text/javascript">
	function callprint(){
		var css = '@page { size: landscape; }',
		head = document.head || document.getElementsByTagName('head')[0],
		style = document.createElement('style');

		style.type = 'text/css';
		style.media = 'print';

		if (style.styleSheet){
		style.styleSheet.cssText = css;
		} else {
		style.appendChild(document.createTextNode(css));
		}

		head.appendChild(style);

		window.print();
	}
</script>

<script type="text/javascript">
	function update(id){
 //    $('#loadicon').show(); 
	// $("#btnSubmit").attr("disabled", true);
 //           var id = id;
 //           $.ajax({
 //                url:"rc_api_callzoop_again.php",  
 //                method:"post",  
 //                data:{id:id},  
 //                success:function(data){  
 //        			 $('#response').html(data);  
 //      				 // $("#btnSubmit").attr("disabled", false);
 //                     $('#loadicon').hide(); 
 //                }
 //           });
    }
</script>
</head>


	
<body style="overflow-x: auto !important;font-family: 'Open Sans', sans-serif !important" onkeypress="return disableCtrlKeyCombination(event);" onkeydown = "return disableCtrlKeyCombination(event);">


<div id="response"></div>
<button type="button" onclick="javascript:location.href='rc_api_index.php'" style="margin-left:20px;margin-top:10px;" class="btn btn-sm btn-danger"> <span class="glyphicon glyphicon-remove"> </span> CLOSE </button>

<button onclick="callprint()" style="margin-top:10px;margin-left:10px" class="btn btn-sm btn-primary"><span class="glyphicon glyphicon-file"> </span> PRINT RC</button>


<!-- <button onclick="update('')" id="btnSubmit" style="float: right; margin-top:10px;margin-right:20px" class="btn btn-sm btn-warning"> <span class="glyphicon glyphicon-repeat"> </span>  UPDATE (Recheck RC) </button> -->

<div class="container-fluid">

<div class="row">
	
	<div class="form-group col-md-4"></div>		
	
	<div class="form-group col-md-4">
		<center><span style="font-weight: bold; font-size: 18px;">Registration No: <?php echo $id; ?> </span> </center>
	</div>
	
	 <div class="form-group col-md-4" style="font-size:12px;">
		<span class="pull-right"></span>
	</div>

</div>
 
<br />
<div class="row">
<style type="text/css">

@media all {    
    .watermark {
        display: inline;
        position: fixed !important;
        opacity: 0.1;
        font-size: 5em;
        font-weight: bold;
        width: 100%;
        text-align: center;
        z-index: 1000;
        top: 350px;
        right:150px;
        transform:rotate(330deg);
    -webkit-transform:rotate(330deg);
    }
}
</style>
<?php


?>
	<div class="watermark">RAMAN ROADWAYS PVT LTD</div>

<div class="col-md-12 table-responsive" style='overflow-x:auto'>
<table class="alldata" border="0" style="width:100%;font-size:12px">
<tr>
<td class="col-md-6" colspan="2"><label>RC Last Updated at </label> <?php echo $row['request_timestamp']; ?> <label>by user </label> <?php echo $row['branch_user']; ?> <label>of branch</label> <?php echo $row['branch']; ?> </td>
</tr>
<tr>
<td class="col-md-6"><label>Vehicle Blacklist status: </label> <?php echo $row['rc_blacklist_status']; ?> </td>
<td class="col-md-6"><label>Body Type of the Vehicle: </label> <?php echo $row['rc_body_type_desc']; ?> </td>
</tr><tr>
<td class="col-md-6"><label>Chassis Number of the Vehicle: </label> <?php echo $row['rc_chasi_no']; ?> </td>
<td class="col-md-6"><label>Registered Color of the Vehicle: </label> <?php echo $row['rc_color']; ?> </td>
</tr><tr>
<td class="col-md-6"><label>Registered Color of the Vehicle: </label> <?php echo $row['rc_cubic_cap']; ?> </td>
<td class="col-md-6"><label>Engine Number of the vehicle: </label> <?php echo $row['rc_eng_no']; ?> </td>
</tr><tr>
<td class="col-md-6"><label>Father's Name of Registered Owner of the vehicle: </label> <?php echo $row['rc_f_name']; ?> </td>
<td class="col-md-6"><label>Name of Vehicle Financier: </label> <?php echo $row['rc_financer']; ?> </td>
</tr><tr>
<td class="col-md-6"><label>Date of Validity of Vehicle Fitness certificate: </label> <?php if($row['rc_fit_upto']!=NULL) echo date('d-M-Y', strtotime($row['rc_fit_upto'])); ?> </td>
<td class="col-md-6"><label>Vehicle Fuel Type: </label> <?php echo $row['rc_fuel_desc']; ?> </td>
</tr><tr>
<td class="col-md-6"><label>Gross Weight of the Vehicle: </label> <?php echo $row['rc_gvw']; ?> </td>
<td class="col-md-6"><label>Insurer Name of the Vehicle: </label> <?php echo $row['rc_insurance_comp']; ?> </td>
</tr><tr>
<td class="col-md-6"><label>Insurance Policy Number of the Vehicle: </label> <?php echo $row['rc_insurance_policy_no']; ?> </td>
<td class="col-md-6"><label>Date of validity of RC Insurance: </label> <?php if($row['rc_insurance_upto']!=NULL) echo date('d-M-Y', strtotime($row['rc_insurance_upto'])); ?> </td>
</tr><tr>
<td class="col-md-6"><label>Name of Vehicle Manufacturer: </label> <?php echo $row['rc_maker_desc']; ?> </td>
<td class="col-md-6"><label>Vehicle Model and Make: </label> <?php echo $row['rc_maker_model']; ?> </td>
</tr><tr>
<td class="col-md-6"><label>Month & Year of Vehicle Manufacture: </label> <?php echo $row['rc_manu_month_yr']; ?> </td>
<td class="col-md-6"><label>Registered owner Mobile Number: </label> <?php echo $row['rc_mobile_no']; ?> </td>
</tr><tr>
<td class="col-md-6"><label>Number of Cylinders: </label> <?php echo $row['rc_no_cyl']; ?> </td>
<td class="col-md-6"><label>No objection certificate in case of vehicle transfer from one state to another: </label> <?php echo $row['rc_noc_details']; ?> </td>
</tr><tr>
<td class="col-md-6"><label>When NOC was issued: </label> <?php if($row['rc_non_use_from']!=NULL) echo date('d-M-Y', strtotime($row['rc_non_use_from'])); ?> </td>
<td class="col-md-6"><label>No objection certificate status: </label> <?php echo $row['rc_non_use_status']; ?> </td>
</tr><tr>
<td class="col-md-6"><label>Expiry date of NOC: </label> <?php if($row['rc_non_use_to']!=NULL) echo date('d-M-Y', strtotime($row['rc_non_use_to'])); ?> </td>
<td class="col-md-6"><label>Vehicle Pollution Norms Description: </label> <?php echo $row['rc_norms_desc']; ?> </td>
</tr><tr>
<td class="col-md-6"><label>National permit issued by RTO: </label> <?php echo $row['rc_np_issued_by']; ?> </td>
<td class="col-md-6"><label>National permit issued number: </label> <?php echo $row['rc_np_no']; ?> </td>
</tr><tr>
<td class="col-md-6"><label>Validity of national permit: </label> <?php if($row['rc_np_upto']!=NULL) echo date('d-M-Y', strtotime($row['rc_np_upto'])); ?> </td>
<td class="col-md-6"><label>Registered Name of Owner: </label> <?php echo $row['rc_owner_name']; ?> </td>
</tr><tr>
	<td class="col-md-6" colspan="2"><label>Registered Permanent Address of the Vehicle Owner: </label> <?php echo $row['rc_permanent_address']; ?> </td>
</tr>
<tr>
	<td class="col-md-6" colspan="2"><label>Registered Present Address of the Vehicle Owner: </label> <?php echo $row['rc_present_address']; ?> </td>
</tr>
<tr>
<td class="col-md-6"><label>Owner serial no.: </label> <?php echo $row['rc_owner_sr']; ?> </td>
<td class="col-md-6"><label>Permit issue date: </label><?php if($row['rc_permit_issue_dt']!=NULL) echo date('d-M-Y', strtotime($row['rc_permit_issue_dt'])); ?></td>
</tr><tr>
<td class="col-md-6"><label>Permit number: </label> <?php echo $row['rc_permit_no']; ?> </td>
<td class="col-md-6"><label>Type of Permit issued for the vehicle: </label> <?php echo $row['rc_permit_type']; ?> </td>
</tr><tr>
<td class="col-md-6"><label>Date of issue of Permit of the vehicle: </label> <?php if($row['rc_permit_valid_from']!=NULL) echo date('d-M-Y', strtotime($row['rc_permit_valid_from'])); ?> </td>
<td class="col-md-6"><label>Date of validity of Permit of the vehicle upto: </label> <?php if($row['rc_permit_valid_upto']!=NULL) echo date('d-M-Y', strtotime($row['rc_permit_valid_upto'])); ?> </td>
</tr><tr>
<td class="col-md-6"><label>PUC Registration Number of the vehicle: </label> <?php echo $row['rc_pucc_no']; ?> </td>
<td class="col-md-6"><label>Expiry date of PUC certificate of the vehicle: </label> <?php if($row['rc_pucc_upto']!=NULL) echo date('d-M-Y', strtotime($row['rc_pucc_upto'])); ?> </td>
</tr><tr>
<td class="col-md-6"><label>Location of RTO where the vehicle was registered: </label> <?php echo $row['rc_registered_at']; ?> </td>
<td class="col-md-6"><label>Date of Registration of the Vehicle: </label> <?php if($row['rc_regn_dt']!=NULL) echo date('d-M-Y', strtotime($row['rc_regn_dt'])); ?> </td>
</tr><tr>
<td class="col-md-6"><label>Registration number of the Vehicle: </label> <?php echo $row['rc_regn_no']; ?> </td>
<td class="col-md-6"><label>Vehicle Passenger Seating Capacity: </label> <?php echo $row['rc_seat_cap']; ?> </td>
</tr><tr>
<td class="col-md-6"><label>Maximum Sleeper Capacity: </label> <?php echo $row['rc_sleeper_cap']; ?> </td>
<td class="col-md-6"><label>Capacity of Standing Passengers in the Vehicle: </label> <?php echo $row['rc_stand_cap']; ?> </td>
</tr><tr>
<td class="col-md-6"><label>Active/NOC issued/De registered: </label> <?php echo $row['rc_status']; ?> </td>
<td class="col-md-6"><label>Date of RC Status Verification: </label> <?php if($row['rc_status_as_on']!=NULL) echo date('d-M-Y', strtotime($row['rc_status_as_on'])); ?> </td>
</tr><tr>
<td class="col-md-6"><label>Duration till the Tax on the Vehicle has been paid (Life time / One time): </label> <?php if($row['rc_tax_upto']!='LTT') { echo date('d-M-Y', strtotime($row['rc_tax_upto'])); } else { echo 'LTT'; } ?>
 </td>
<td class="col-md-6"><label>Unladden Weight of the Vehicle: </label> <?php echo $row['rc_unld_wt']; ?> </td>
</tr><tr>
<td class="col-md-6"><label>Vehicle category: </label> <?php echo $row['rc_vch_catg']; ?> </td>
<td class="col-md-6"><label>Description of Vehicle Class: </label> <?php echo $row['rc_vh_class_desc']; ?> </td>
</tr><tr>
<td class="col-md-6"><label>Wheelbase in mm of the vehicle: </label> <?php echo $row['rc_wheelbase']; ?> </td>
<td class="col-md-6"><label>State code: </label> <?php echo $row['state_cd']; ?> </td>
</tr>

</table>
</div>
</div>

<br />
  
</div>
	
</body>
</html>
