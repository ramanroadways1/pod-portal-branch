<?php 
  require 'header.php';
?>   
<style> 
.table-hover tbody tr:hover td,.table-hover tbody tr:hover th{background-color:#ffedda}.table td{vertical-align:middle!important;font-size:11px!important;color:#000;font-family:Verdana,Geneva,sans-serif;padding-top:4px;padding-right:4px;padding-bottom:4px;padding-left:10px}.table-bordered td{border:3px solid #e3e6f0}#user_data_info,#user_data_length{float:left}#user_data_filter,#user_data_paginate{float:right}.paginate_button{color:#000;float:left;padding:6px 12px;text-decoration:none;border:1px solid #ccc;cursor:pointer}.ellipsis{display:none}[type=search]{margin-right:10px; width: 250px; }.ui-autocomplete{z-index:2150000000!important}.container input{position:absolute;opacity:0;cursor:pointer;height:0;width:0}.checkmark{border-radius:2px;position:absolute;top:0;height:20px;width:20px;background-color:#fff;border:1px solid #000}.container:hover input~.checkmark{background-color:#fff}.container input:checked~.checkmark{background-color:#fff}.container input:disabled~.checkmark{background-color:#eaecf4}.checkmark:after{content:"";position:absolute;display:none}.container input:checked~.checkmark:after{display:block}.container .checkmark:after{left:6px;top:-1px;width:8px;height:16px;border:solid #000;border-width:0 3px 3px 0;-webkit-transform:rotate(45deg);-ms-transform:rotate(45deg);transform:rotate(45deg)}button:disabled,button[disabled]{border:1px solid #333!important;color:#333!important;cursor:no-drop} .table .thead-light th{text-align: center; font-size: 11px; color:#444; text-transform: uppercase; } .component{display: none;} 
	table {width: 100% !important;} table.table-bordered.dataTable td { white-space: nowrap; overflow: hidden; text-overflow:ellipsis;  }

	/* This css is for normalizing styles. You can skip this. */
*, *:before, *:after {
  -webkit-box-sizing: border-box;
  -moz-box-sizing: border-box;
  box-sizing: border-box;
  margin: 0;
  padding: 0;
}


td:nth-child(1){
	padding: 0px !important;
	width: 120px;
}

.new {
/*  padding: 50px;
*/}

.form-group {
  display: block;
  margin-bottom: 15px;
}

.form-group input {
  padding: 0;
  height: initial;
  width: initial;
  margin-bottom: 0;
  display: none;
  cursor: pointer;
}

.form-group label {
  position: relative;
  cursor: pointer;
}

.form-group label:before {
  content:'';
  -webkit-appearance: none;
  background-color: transparent;
  border: 2px solid #004973;
  background-color: #FBFBFB;
  box-shadow: 0 1px 2px rgba(0, 0, 0, 0.05), inset 0px -15px 10px -12px rgba(0, 0, 0, 0.05);
  padding: 8px;
  display: inline-block;
  position: relative;
  vertical-align: middle;
  cursor: pointer;
  margin-right: 5px;
}

.form-group input:checked + label:after {
  content: '';
  display: block;
  position: absolute;
  top: 2px;
  left: 7px;
  width: 6px;
  height: 13px;
  border: solid #004973;
  border-width: 0 2px 2px 0;
  transform: rotate(45deg);
}

input{
	text-transform: uppercase;
}



#appenddiv, #appenddiv2 {
    display: block; 
    position:relative
} 
.ui-autocomplete {
    position: absolute;
}
</style>
  

<div id="response"></div> 

<form id="save" action="" method="post" autocomplete="off">
<div class="row"> 
<div class="col-md-12"> <h3 style="float: left; margin-top: 10px;"> POD Inward (जो पोहोच अपनी ब्रांच पर कलेक्ट करना पेंडिंग हो)</h3> </div>

<div class="col-md-12" >
<div class="card shadow mb-4"> 
 <div class="card-body table-responsive  ">

  <table id="user_data" class="table table-bordered table-hover" style="">
      <thead class="thead-light">
        <tr>  
        <th> # </th>  
        <th > Memo_No </th>
        <th > From_Station </th>
        <th > Destination </th>
        <th > Dispatch_Date </th>
        <th > Sended_By </th>
        <th > Narration </th>
        <th > Outstanding </th>
        </tr>
      </thead>
  
  </table>

  
</div>  
</div>
</div> 
</div> 
</form>
 
<script type="text/javascript"> 

jQuery( document ).ready(function() {

$('#loadicon').show(); 
var table = jQuery('#user_data').dataTable({
     "lengthMenu": [ [7, 500, 1000, -1], [7, 500, 1000, "All"] ], 
     "bProcessing": true,
     "sAjaxSource": "inward_fetch.php",
      "bPaginate": true,
      "sPaginationType":"full_numbers",
      "iDisplayLength": 7,
      //"order": [[ 8, "desc" ]],
      "columnDefs":[
      {
        "targets":[0, 1, 2, 3, 4, 5, 6, 7],
        "orderable":false,
      },
      ],
      "aoColumns": [
        { mData: '0' },
        { mData: '1' } ,
        { mData: '2' },
        { mData: '3' },
        { mData: '4' },
        { mData: '5' },
        { mData: '6' }, 
        { mData: '7' }
    ],
    "initComplete": function( settings, json ) {
    $('#loadicon').hide();
    }
});  
   
});   
</script>
<script type="text/javascript">
$(document).ready(function() { 
    var table = $('#user_data').DataTable(); 
} );
</script>   
<?php 
  include 'footer.php';
?>  
 