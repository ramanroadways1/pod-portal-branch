<?php
  require('connect.php');
  
  $id = $conn->real_escape_string($_REQUEST['id']);

  $connection = new PDO('mysql:host='.$host.';dbname='.$db_name.';', $username, $password );
  $statement = $connection->prepare("
SELECT rcv_pod.veh_type, rcv_pod.frno, rcv_pod.id, rcv_pod.lrno, rcv_pod.pod_copy, rcv_pod.pod_date, rcv_pod.billing, rcv_pod.colset, rcv_pod.consignor_id,    lr_sample.consignee,  



   if(m.frmstn is NULL,lr_sample.fstation,m.frmstn) as fstation, 
   if(m.tostn is NULL,lr_sample.tstation,m.tostn) as tstation, 

   if(m.lrdate is NULL,lr_sample.date,m.lrdate) as lrdate, 

    if(m.billing_party is NULL,lr_sample.consignor,m.billing_party) as consignor,

   if(m.tno is NULL,lr_sample.truck_no,m.tno) as truck_no 



FROM rcv_pod 
left join lr_sample  on lr_sample.lrno = rcv_pod.lrno 
   left join mkt_bilty m on m.bilty_no = rcv_pod.lrno 

where  rcv_pod.tostation='$branchuser' AND rcv_pod.memono='$id' and rcv_pod.billing='1' and (rcv_pod.colset='' or rcv_pod.colset='-1')"); //rcv_pod.nullify='0' and rcv_pod.self='0' and
  $statement->execute();
  $result = $statement->fetchAll();
  $count = $statement->rowCount();
  $data = array();

$sno = 0;
foreach($result as $row)
{ 
  $sno=$sno+1;
  $sub_array = array(); 
 
  $sub_array[] = "<center>".$sno."</center>";
  $sub_array[] = $row["frno"]; 
  $sub_array[] = $row["lrno"]; 
  $sub_array[] = date('d/m/Y', strtotime($row['lrdate']));
  $sub_array[] = $row["truck_no"]; 
  $sub_array[] = $row["fstation"]; 
  $sub_array[] = $row["tstation"]; 
  $sub_array[] = $row["consignor"]; 

$pod_files1 = array(); 
$copy_no = 0;
foreach(explode(",",$row['pod_copy']) as $pod_copies)
{
  $copy_no++;
        
        if (strpos($pod_copies, 'pdf') !== false) {
        $file = 'PDF';
        } else {
        $file = 'IMAGE';
        }

    if($row['veh_type']=="MARKET"){
      $pod_files1[] = "<center><a href='https://rrpl.online/b5aY6EZzK52NA8F/$pod_copies' target='_blank'>$file: $copy_no</a></center>";
    } else {
      $pod_files1[] = "<a href='https://rrpl.online/diary/close_trip/$pod_copies' target='_blank'>$file: $copy_no</a>";
    }
 }
  $sub_array[] = implode(", ",$pod_files1);
  $sub_array[] = date('d/m/Y', strtotime($row['pod_date']));
  
  $sub_array[] = "<center> <button onclick='action(".$row['id'].", this.id, \"$id\"); this.disabled=true; this.value=\"loading…\";' id='select' class='btn btn-sm btn-success' > <i class='fa fa-check '></i>  ACCEPT  </button> </center>"; 

  $sub_array[] = "<center> <button onclick='action(".$row['id'].", this.id, \"$id\"); this.disabled=true; this.value=\"loading…\";' id='reject' class='btn btn-sm btn-danger'> <i class='fa fa-trash '></i>   REJECT  </button> </center>  ";   

  $data[] = $sub_array;

} 

$results = array(
  "sEcho" => 1,
    "iTotalRecords" => $count,
    "iTotalDisplayRecords" => $count,
    "aaData"=>$data);

echo json_encode($results); 
exit
?>
 