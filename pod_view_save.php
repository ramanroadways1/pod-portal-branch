<?php
require('connect.php');

  // $sendby = $conn->real_escape_string(strtoupper($_POST['sendby']));
  // $date = $conn->real_escape_string(strtoupper($_POST['date'])); 

  $billbranch = $conn->real_escape_string(strtoupper($_POST['billbranch']));
  $billparty = $conn->real_escape_string(strtoupper($_POST['billparty']));
  $billpartyid = $conn->real_escape_string(strtoupper($_POST['billpartyid']));
 
  $dispatchto = $conn->real_escape_string($_POST['dispatchto']);
  //$otherbranch = $conn->real_escape_string(strtoupper($_POST['otherbranch']));
  //$outmemono = $conn->real_escape_string(strtoupper($_POST['outmemono']));

if(isset($_POST['otherbranch']))
{
  $otherbranch = $conn->real_escape_string(strtoupper($_POST['otherbranch']));
} else {
  $otherbranch = "NA";
}

if(isset($_POST['outmemono']))
{
  $outmemono = $conn->real_escape_string(strtoupper($_POST['outmemono']));
} else {
  $outmemono = "NA";
}

if(isset($_POST['sendby']))
{
  $sendby = $conn->real_escape_string(strtoupper($_POST['sendby']));
} else {
  $sendby = "NA";
}
 
if(isset($_POST['date']))
{
  $date = $conn->real_escape_string(strtoupper($_POST['date']));
} else {
  $date = "NA";
}


if(isset($_POST['val1']))
{
  $val1 = $conn->real_escape_string(strtoupper($_POST['val1']));
} else {
  $val1 = "";
}

if(isset($_POST['val2']))
{
  $val2 = $conn->real_escape_string(strtoupper($_POST['val2']));
} else {
  $val2 = "";
}

if(isset($_POST['val3']))
{
  $val3 = $conn->real_escape_string(strtoupper($_POST['val3']));
} else {
  $val3 = "";
}

if(isset($_POST['val4']))
{
  $val4 = $conn->real_escape_string(strtoupper($_POST['val4']));
} else {
  $val4 = "";
}

if(isset($_POST['val5']))
{
  $val5 = $conn->real_escape_string(strtoupper($_POST['val5']));
} else {
  $val5 = "";
}

if(isset($_POST['val6']))
{
  $val6 = $conn->real_escape_string(strtoupper($_POST['val6']));
} else {
  $val6 = "";
}

if(isset($_POST['val7']))
{
  $val7 = $conn->real_escape_string(strtoupper($_POST['val7']));
} else {
  $val7 = "";
}

if(isset($_POST['val8']))
{
  $val8 = $conn->real_escape_string(strtoupper($_POST['val8']));
} else {
  $val8 = "";
}


	try {
	$conn->query("START TRANSACTION"); 
  
if(!empty($_POST['mark']) || !empty($_POST['mark2']) || !empty($_POST['mark3']))
{ 


	if($sendby=="COURIER"){

		if(empty($val1)){
				throw new Exception(" Courier detail is Missing ! "); 
		}

	} else if($sendby=="TRUCK"){

		if(empty($val5) or empty($val6) or empty($val7)){
				throw new Exception(" Truck detail is Missing ! "); 
		}

	} else if($sendby=="PERSON"){

		if(empty($val3) or empty($val4)){
				throw new Exception(" Person detail is Missing ! "); 
		}

	} else if($sendby=="OTHERS"){

		if(empty($val8)){
				throw new Exception(" Others detail is Missing ! "); 
		}
	} 

	$todaydate = date('Y-m-d');
 	$sqli = $conn->query("select count(id) from podmemo where date(dispatchdate)='$todaydate' ");
 	$resi = $sqli->fetch_assoc();
 	$getlast = $resi['count(id)']+1;
 
    $narration = "NA";
  if($sendby=="COURIER"){
    $narration = "Courier Name: ".$val1." / Docket No: ".$val2;
  } else if($sendby=="TRUCK"){
    $narration = "Truck No: ".$val5." / Driver Name: ".$val6." / Driver Mobile: ".$val7;
  } else if($sendby=="PERSON"){
    $narration = "Person Name: ".$val3." / Person Mobile: ".$val4;
  } else if($sendby=="OTHERS"){
    $narration = "Others: ".$val8;
  } 
  
	if($dispatchto=="otherbranch"){
		$billbranch = $otherbranch;
	}

	if($dispatchto=="outmemono"){
		$msql = "select * from podmemo where memono='$outmemono' lock in share mode";
		$mres = $conn->query($msql);
		$mrow = $mres->fetch_assoc();

		if($mrow['status']!='0'){
				throw new Exception(" Intermemo already collected at branch !"); 
		}

		$newmemo = $outmemono;
		$billbranch = $mrow['bill_branch'];

	}
	
	$memo = substr($branchuser, 0, 3).substr($billbranch, 0, 3).date('dmy').$getlast;
	if($dispatchto=="outmemono"){
		$memo = $newmemo;
	}

			$billbranch = strtoupper($billbranch);
			$memo = strtoupper($memo);
		  	$totallr = $totalbty = $totaltrip = "0";

		  	if(!empty($_POST['mark'])){
				$totallr = count($_POST['mark']); 
				$all_lrs = array(); 
				foreach ($_POST['mark'] as $id) { 
					$all_lrs[] = $id; 
				}
				$all_lrs = array_unique($all_lrs);
				$lrs = "'" . implode ( "', '", $all_lrs ) . "'";
				$sql = "UPDATE rcv_pod SET tostation='$billbranch', billing='1', colset='', memono='$memo', billing_time=now() WHERE id in ($lrs)";
				if ($conn->query($sql) === FALSE) {
				throw new Exception(" Update failed for LR Dispatch !"); 
				}
			} else {
				$lrs = "";
			} 

		  	if(!empty($_POST['mark2'])){
				$totalbty = count($_POST['mark2']); 
				$all_bty = array();
				foreach ($_POST['mark2'] as $id) { 
					$all_bty[] = $id; 
				}
				$all_bty = array_unique($all_bty);
				$bty = "'" . implode ( "', '", $all_bty ) . "'";
				$sql = "UPDATE rcv_pod SET tostation='$billbranch', billing='1', colset='', memono='$memo', billing_time=now() WHERE id in ($bty)";
				if ($conn->query($sql) === FALSE) {
				throw new Exception(" Update failed for Bilty Dispatch !"); 
				}
			} else {
				$bty = "";
			}


			if(!empty($_POST['mark3'])){
				$totaltrip = count($_POST['mark3']); 
				$all_trip = array();
				foreach ($_POST['mark3'] as $id) { 
					$all_trip[] = $id; 
				}
				$all_trip = array_unique($all_trip);
				$trip = "'" . implode ( "', '", $all_trip ) . "'";
				$sql = "UPDATE dairy.opening_closing SET tostation='$billbranch', hisab_sent='1', collect='0', memono='$memo', hisab_sent_timestamp=now() WHERE id in ($trip)";
				if ($conn->query($sql) === FALSE) {
				throw new Exception(" Update failed for Trip Dispatch !"); 
				}
		 	} else {
				$trip = "";
			}

 
 	if($billbranch==$branchuser){
			throw new Exception(" Billing Branch cannot be same as POD Branch !"); 
 	}

	$total = $totallr + $totalbty;

	if($dispatchto!='outmemono'){
	 	$sql = "INSERT INTO podmemo (empid, memodate, memono, branch, bill_branch, dispatchdate, sentby, couriername, docketno, narration, contactname, contactmobile, truckno, drivername, drivermobile, remainLR, remainTrip) VALUES ('$empid', '$date', '$memo', '$branchuser', '$billbranch', '$sysdatetime', '$sendby', '$val1', '$val2', '$val8', '$val3', '$val4', '$val5', '$val6','$val7', '$total','$totaltrip')";
		if ($conn->query($sql) === FALSE) {
			throw new Exception(" Insert failed in PODMEMO !"); 
		} 
	} else {

		$total = $total + $mrow['remainLR'];
		$totaltrip = $totaltrip + $mrow['remainTrip'];

		$sql = "update podmemo set remainLR='$total', remainTrip='$totaltrip' where memono='$memo'";
		if ($conn->query($sql) === FALSE) {
			throw new Exception(" Update failed in PODMEMO !"); 
		} 
	}

  	// here value will repeat so to delete - user blocking
	$sql = "delete from podtrack where memono='$memo'";
	if ($conn->query($sql) === FALSE) {
		throw new Exception(" Delete failed in PODTRACK for LR !"); 
	}

 	$sql = "insert into podtrack (`lrno`, `lrid`, `frno`, `lrtype`,  `dispatchbranch`, `collectbranch`, `dispatch`, `dispatchdate`, `dispatchusr`, `memono`) select lrno, id, frno, veh_type, fromstation, tostation, '1', '$sysdatetime', '$empid', memono from rcv_pod where memono='$memo'";
	if ($conn->query($sql) === FALSE) {
		throw new Exception(" Insert failed in PODTRACK for LR !"); 
	}
	
	$sql = "insert into podtrack (`lrno`, `lrid`, `frno`, `lrtype`,  `dispatchbranch`, `collectbranch`, `dispatch`, `dispatchdate`, `dispatchusr`, `memono`) select trip_no, id, trip_no, 'TRIP', fromstation, tostation, '1', '$sysdatetime', '$empid', memono from dairy.opening_closing where memono='$memo'";
	if ($conn->query($sql) === FALSE) {
		throw new Exception(" Insert failed in PODTRACK for Trip !"); 
	}

	} else {
		throw new Exception(" Please select LR !"); 
	}

 
		$conn->query("COMMIT");
		echo "
		<script>
		Swal.fire({
		position: 'top-end',
		icon: 'success',
		title: 'Intermemo Updated.',
		showConfirmButton: false,
		timer: 1500
		})
		</script>"; 
		}
		catch(Exception $e) {
				$conn->query("ROLLBACK"); 
				$content = $e->getMessage();
				$content = preg_replace("/[^0-9a-zA-Z ]/", "", $content);  
				echo "
				<script>
				Swal.fire({
				icon: 'error',
				title: 'Error !!!',
				text: '".$content."'
				})
				</script>";
		}
 