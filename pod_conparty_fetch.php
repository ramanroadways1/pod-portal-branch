<?php require('connect.php'); ?>

	<style type="text/css">
	.applyBtn{border-radius:0!important}.show-calendar{top:180px!important}.applyBtn{border-radius:0!important}table.table-bordered.dataTable td{padding:10px 5px 10px 10px}.dt-buttons{float:right}.user_data_filter{float:right}.dt-button{padding:4px 20px;text-transform:uppercase;font-size:11px;text-align:center;cursor:pointer;outline:0;color:#fff;background-color:#37474f;border:none;border-radius:2px;box-shadow:0 3px #999}.dt-button:hover{background-color:#3e8e41}.dt-button:active{background-color:#3e8e41;box-shadow:0 5px #666;transform:translateY(4px)}#user_data_wrapper{width:100%!important}.dt-buttons{margin-bottom:20px}.table-hover tbody tr:hover td,.table-hover tbody tr:hover th{background-color:#ffedda}.table td{vertical-align:middle!important;font-size:11px!important;color:#000;font-family:Verdana,Geneva,sans-serif;padding-top:4px;padding-right:4px;padding-bottom:4px;padding-left:10px}.table-bordered td{border:1px solid #e3e6f0}#user_data_info,#user_data_length{float:left}#user_data_filter,#user_data_paginate{float:right}.paginate_button{color:#000;float:left;padding:6px 12px;text-decoration:none;border:1px solid #ccc;cursor:pointer}.ellipsis{display:none}[type=search]{margin-right:10px;width:250px}.ui-autocomplete{z-index:2150000000!important}.container input{position:absolute;opacity:0;cursor:pointer;height:0;width:0}.checkmark{border-radius:2px;position:absolute;top:0;height:20px;width:20px;background-color:#fff;border:1px solid #000}.container:hover input~.checkmark{background-color:#fff}.container input:checked~.checkmark{background-color:#fff}.container input:disabled~.checkmark{background-color:#eaecf4}.checkmark:after{content:"";position:absolute;display:none}.container input:checked~.checkmark:after{display:block}.container .checkmark:after{left:6px;top:-1px;width:8px;height:16px;border:solid #000;border-width:0 3px 3px 0;-webkit-transform:rotate(45deg);-ms-transform:rotate(45deg);transform:rotate(45deg)}button:disabled,button[disabled]{border:1px solid #333!important;color:#333!important;cursor:no-drop}.table .thead-light th{text-align:center;font-size:11px;color:#444;text-transform:uppercase}.component{display:none}table{width:100%!important}table.table-bordered.dataTable td{white-space:nowrap;overflow:hidden;text-overflow:ellipsis}
	</style>


	<div class="row">
	<div class="col-md-7">
	<div class="card shadow"> 
	<div class="card-header">
	<div class="card-body table-responsive" style="overflow:auto">
	<table class="table table-bordered table-hover" style="background-color: #fff;">
	<thead class="thead-light">
	<th style="text-align: center;">Sno</th>
	<th style="text-align: center;">Billing Branch</th>
	<!-- <th style="text-align: center;">Billing Party</th> -->
	<th style="text-align: center;">No of LR's</th>
	<th style="text-align: center;">#</th>
	</thead>							
	<?php
	$sql = $conn->query("(SELECT COUNT(id) as total, tostation FROM rcv_pod where nullify='0' and self='0' and fromstation='$branchuser' AND billing!='1' AND billing_party!='0' and tostation!='' GROUP BY tostation)
	UNION
	(SELECT COUNT(id) as total, if(tostation is null,'CGROAD',tostation) as tostation FROM rcv_pod where nullify='0' and self='0' and  
	fromstation='$branchuser' AND billing!='1' AND billing_party!='0' and tostation='CGROAD') order by tostation desc");
	
	if(!$sql){
		// errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
		exit();
	}
 

	if($sql->num_rows>0)
	{
		$i=1;
		while($res = $sql->fetch_assoc()){
			
			$isql = "select count(id) as cnt from dairy.opening_closing where nullify='0' and hisab_sent!='1' and fromstation='$branchuser' and tostation='$res[tostation]'";
			$ires = $conn->query($isql);
			$irow = $ires->fetch_assoc();
			$cont = $irow['cnt'];

			$tot = $res['total'] + $cont; //+ $xrow['cnt'];

			echo "<tr> <td style='font-size:12px !important; color: #444 !important;'> <center> (".$i.") </center> </td>";
			echo " <td style='font-size:14px !important; color: #444 !important;'> <center> ".$res['tostation']." </center></td>";
			// echo " <td> ".$row['name']." </td>";
			echo " <td style='font-size:14px !important; color: #444 !important;'> <center> ".$tot." </center></td>";
			echo ' <td style="text-align: center; "> <a style="font-size:11px"  href="pod_view.php?id='.$res["tostation"].'" class="btn btn-sm btn-warning"> <i class="fa fa-file-text-o" aria-hidden="true"></i> View  </a> </td> </tr>';
			$i=$i+1;
		}

		} else {
			echo "<tr> <td colspan='5'> No Records Found ! </td> </tr>";
		}
		?>
		</table>
		</div>
		</div>
		</div>
		</div>
		</div>
