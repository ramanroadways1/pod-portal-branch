<?php

require('connect.php'); 

  $id = $conn->real_escape_string($_REQUEST['id']); 
  $action = $conn->real_escape_string($_REQUEST['act']); 
  $memo = $conn->real_escape_string($_REQUEST['memo']); 
 
	try {
	$conn->query("START TRANSACTION"); 

		if($action=="select"){
 
			$sql = "select colset from rcv_pod where id='$id'";
			if ($conn->query($sql) === FALSE) {
					throw new Exception("Unable to Fetch pod status (Accept)"); 
			}
			$row = $conn->query($sql);
			$res = $row->fetch_assoc();
			if($res['colset']=="-1"){
					throw new Exception("POD already Rejected"); 
			}


			$qry = "UPDATE rcv_pod SET reachdestiny=if(billing_ofc=tostation,'1','0'), colset='1', colset_time='$sysdatetime' WHERE id='$id' and billing='1' and (colset='' or colset='-1' )";
			if ($conn->query($qry) === FALSE) {
				throw new Exception("Update failed on rcv_pod !"); 
			} else {
				$qry = "UPDATE podmemo set status='1', remainlr=remainlr-1, collectdate='$sysdatetime'  where memono='$memo'";
				if ($conn->query($qry) === FALSE) {
					throw new Exception("Update failed on podmemo !"); 
				}

				$qry = "update podtrack set collect='1', collectdate='$sysdatetime', collectusr='$empid' where lrid='$id' and memono='$memo' and collect='0'";
				if ($conn->query($qry) === FALSE) {
					throw new Exception("Update failed on podtrack !"); 
				}

				$qry = "select * from rcv_pod where id='$id'";
				if ($conn->query($qry) === FALSE) {
					throw new Exception(""); 
				}else{
					$resp = $conn->query($qry);
					$rowp = $resp->fetch_assoc();
					if($rowp['billing_ofc']!=$rowp['tostation']){
						
						$sql = "insert into podforward (`lrid`, `lrtype`, `lrno`, `frno`, `oldfrom`, `oldto`, `newfrom`, `newto`, `memono`, `dispatchtime`, `collecttime`) values ('$rowp[id]','$rowp[veh_type]','$rowp[lrno]','$rowp[frno]','$rowp[fromstation]','$rowp[tostation]','$rowp[tostation]','$rowp[billing_ofc]','$rowp[memono]','$rowp[billing_time]','$rowp[colset_time]')";
						if ($conn->query($sql) === FALSE) {
							throw new Exception("Insert failed for CarryForward log !"); 
						} 

						$sql = "update rcv_pod set fromstation='$rowp[tostation]', tostation='$rowp[billing_ofc]', colset='', colset_time=NULL, billing='', billing_time=NULL, exdate=now() where id='$id'";
						if ($conn->query($sql) === FALSE) {
							throw new Exception("Update failed for CarryForward rcv_pod !"); 
						} 
					} 
				}
			} 
		 
		} else if ($action=="reject"){

			$sql = "select colset from rcv_pod where id='$id'";
			if ($conn->query($sql) === FALSE) {
					throw new Exception("Unable to Fetch pod status (Reject)"); 
			}
			$row = $conn->query($sql);
			$res = $row->fetch_assoc();
			if($res['colset']=="1"){
					throw new Exception("POD already Accepted"); 
			}


			$qry = "UPDATE rcv_pod SET billing='', colset='-1', colset_time=NULL WHERE id='$id' and billing='1' and (colset='' or colset='-1' )";
			if ($conn->query($qry) === FALSE) {
				throw new Exception("Reject update failed on rcv_pod !"); 
			} else {
				$qry = "UPDATE podmemo set status='1', remainlr=remainlr-1, collectdate='$sysdatetime' where memono='$memo'";
				if ($conn->query($qry) === FALSE) {
					throw new Exception("Reject update failed on podmemo !"); 
				}

				$qry = "UPDATE podtrack set reject='1', collectusr='$empid' where lrid='$id' and memono='$memo' and reject='0'";
				if ($conn->query($qry) === FALSE) {
					throw new Exception("Reject update failed on podtrack !"); 
				}
			} 
 
		} 

		$conn->query("COMMIT");
		// echo "
		// <script>
		// Swal.fire({
		// position: 'top-end',
		// icon: 'success',
		// title: 'LR Updated.',
		// showConfirmButton: false,
		// timer: 1500
		// })
		// </script>";  
	}
	catch(Exception $e) {
		$conn->query("ROLLBACK"); 
		$content = $e->getMessage();
		$content = preg_replace("/[^0-9a-zA-Z ]/", "", $content);  
			echo "
			<script>
			Swal.fire({
			icon: 'error',
			title: 'Error !!!',
			text: '".$content."'
			})
			</script>";
	}


?>

 