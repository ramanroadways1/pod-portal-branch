<?php

  require('connect.php');
 
   $id = $conn->real_escape_string($_REQUEST['id']);
    
    // $sqli = $conn->query("select * from billing_party where id='$id'");
    // $resi = $sqli->fetch_assoc();

	$connection = new PDO('mysql:host='.$host.';dbname='.$db_name.';', $username, $password );
	$statement = $connection->prepare("SELECT r.id, m.frmstn, m.tostn,m.billing_party, m.tno, m.bilty_no, m.plr, m.date as mdate,  r.pod_date as date, r.pod_copy as copy, r.billing as dispatch, r.billing_time as dispatch_time, r.colset as collect
    FROM `mkt_bilty` m left join rcv_pod r on 
	  r.frno = m.bilty_no where r.fromstation='$branchuser' and r.tostation='$id' and r.pod_copy is not null and r.billing!='1'");
  // change to billing office -- m.billing_branch='$id' 
	$statement->execute();
	$result = $statement->fetchAll();
	$count = $statement->rowCount();
	$data = array();

$sno=0;
foreach($result as $row)
{ 
  $sno = $sno+1;
  $sub_array = array(); 
  
  $btn= "<center> <div class='form-group' style='margin:0px !important;'> <input name='mark2[]' type='checkbox' id='".$row["id"]."' value='".$row["id"]."'> <label for='".$row["id"]."'>   </label> </div>   </center> "; 
  $sub_array[] = $btn; 
	$sub_array[] = "<center>".$sno."</center>";
  $sub_array[] = $row["bilty_no"]; 
  $sub_array[] = $row["plr"]; 
  $sub_array[] = date('d/m/Y', strtotime($row['mdate'])); 
  $sub_array[] = $row["tno"]; 
  $sub_array[] = $row["frmstn"]; 
  $sub_array[] = $row["tostn"]; 
  $sub_array[] = $row["billing_party"]; 

$pod_files1 = array(); 
$copy_no = 0;
  foreach(explode(",",$row['copy']) as $pod_copies)
  {
        $copy_no++;
        if (strpos($pod_copies, 'pdf') !== false) {
          $file = 'PDF';
        } else {
          $file = 'IMAGE';
        }
 
      $pod_files1[] = "<a href='https://rrpl.online/diary/close_trip/$pod_copies' target='_blank'>$file: $copy_no</a>";
  }
  $sub_array[] = implode(", ",$pod_files1);
  $sub_array[] = date('d/m/Y', strtotime($row['date']));
  
  $stat = "";
  if($row['dispatch']=="0" && $row['collect']=="-1"){
  $stat = "<font color='red'>CANCEL</font>";
  }else {
  $stat = "NA";
  }
  
  $sub_array[] = $stat; 
 
  $selectparty = "<select style='max-width:150px; min-width:150px;' disabled> <option> -- select -- </option> <option> $id </option> </select>"; 

  // $sub_array[] = $selectparty; 
  $sub_array[] = "<center>
      <a onclick='editbilty(".$row['id'].")' class='btn btn-sm btn-warning' style='color: #fff; padding:2px 8px; margin:0px;'> <i style='font-size: 11px;' class='fa fa-edit '></i> edit </a>
   </center>"; 
 
	$data[] = $sub_array;

} 

$results = array(
	"sEcho" => 1,
    "iTotalRecords" => $count,
    "iTotalDisplayRecords" => $count,
    "aaData"=>$data);

echo json_encode($results); 
exit
?>
 